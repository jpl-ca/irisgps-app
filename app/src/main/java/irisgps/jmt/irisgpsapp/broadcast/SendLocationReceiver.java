package irisgps.jmt.irisgpsapp.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import irisgps.jmt.irisgpsapp.presenter.GpsLocationPresenter;
import irisgps.jmt.irisgpsapp.util.JMStore;
import irisgps.jmt.irisgpsapp.view.interface_view.GeolocationView;

/**
 * Created by JMTech-Android on 27/01/2016.
 */
public class SendLocationReceiver extends BroadcastReceiver implements GeolocationView{
    GpsLocationPresenter gpsLocationPresenter;
    Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        if(new JMStore(context).getBoolean(JMStore.PREF_HAS_SESSION)){
            gpsLocationPresenter = new GpsLocationPresenter(this);
            gpsLocationPresenter.proccessToSendLocations();
        }
    }

    @Override
    public Context getContext() {
        return context ;
    }

    @Override
    public void stopService() {}

    @Override
    public boolean hasFirstLocation() {
        return false;
    }
}