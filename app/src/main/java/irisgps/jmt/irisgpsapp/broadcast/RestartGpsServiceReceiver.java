package irisgps.jmt.irisgpsapp.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import irisgps.jmt.irisgpsapp.service.gps.GpsService;
import irisgps.jmt.irisgpsapp.util.GpsUtil;

/**
 * Created by JMTech-Android on 27/01/2016.
 */
public class RestartGpsServiceReceiver extends BroadcastReceiver{
    public static String ACTION_RESTART = "irisgps.jmt.irisgpsapp.action.restart_gps_service";
    @Override
    public void onReceive(final Context ctx, Intent intent) {
        System.out.println("RestartGpsServiceReceiver :)");
        final Intent IntentGpsService = new Intent(ctx, GpsUtil.getGpsService(ctx));
        ctx.stopService(IntentGpsService);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ctx.startService(IntentGpsService);
            }
        }, 5 * 1000);
    }
}