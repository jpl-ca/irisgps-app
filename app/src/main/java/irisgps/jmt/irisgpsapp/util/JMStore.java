package irisgps.jmt.irisgpsapp.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by JMTech-Android on 22/01/2016.
 */
public class JMStore {
    SharedPreferences DB;
    private String PREF_STATUS_NETWORK = "IS_STATUS_NETWORK";
    private String PREF_LAST_TIME_NETWORK = "IS_LAST_TIME_NETWORK";
    private String PREF_ORGANIZATION_VALIDATE = "IS_PREF_ORGANIZATION_VALIDATE";
    private String PREF_INTERVAL_TIME_GPS = "IS_PREF_INTERVAL_TIME_GPS";
    private String PREF_GPS_LATITUDE = "PREF_GPS_LATITUDE";
    private String PREF_GPS_LONGITUDE = "PREF_GPS_LONGITUDE";
    public static String PREF_HAS_SESSION = "PREF_HAS_SESSION";
    public JMStore(Context ctx){
        DB= ctx.getSharedPreferences("sp_jm_gps", Context.MODE_PRIVATE);
    }
    public void saveString(String key,String value){
        System.out.println("Guardando JMSTORE:"+key+"="+value);
        SharedPreferences.Editor editor = DB.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public String getString(String key) {
        if(DB.contains(key)) {
            return DB.getString(key, "");
        }
        return "";
    }
    public void pushLastTimeNetwork(long value){
        SharedPreferences.Editor editor = DB.edit();
        editor.putLong(PREF_LAST_TIME_NETWORK, value);
        editor.commit();
    }
    public long getLastTimeNetwork() {
        if(DB.contains(PREF_LAST_TIME_NETWORK)) {
            return DB.getLong(PREF_LAST_TIME_NETWORK, 0);
        }
        return 0;
    }
    public void pushIntervalTimeGps(int value){
        SharedPreferences.Editor editor = DB.edit();
        editor.putInt(PREF_INTERVAL_TIME_GPS, value);
        editor.commit();
    }
    public int getIntervalTimeGps() {
        if(DB.contains(PREF_INTERVAL_TIME_GPS)) {
            return DB.getInt(PREF_INTERVAL_TIME_GPS, 5000);
        }
        return 5000;
    }
    public void pushStatusNetwork(int value){
        SharedPreferences.Editor editor = DB.edit();
        editor.putInt(PREF_STATUS_NETWORK, value);
        editor.commit();
    }
    public long getStatusNetwork() {
        if(DB.contains(PREF_STATUS_NETWORK)) {
            return DB.getInt(PREF_STATUS_NETWORK, 0);
        }
        return 0;
    }
    public void pushValidateOrganization(boolean value){
        SharedPreferences.Editor editor = DB.edit();
        editor.putBoolean(PREF_ORGANIZATION_VALIDATE, value);
        editor.commit();
    }
    public boolean getValidateOrganization() {
        if(DB.contains(PREF_ORGANIZATION_VALIDATE)) {
            return DB.getBoolean(PREF_ORGANIZATION_VALIDATE, false);
        }
        return false;
    }

    public int nextInt() {
        int c=1;
        String ky = "next_val";
        if(DB.contains(ky))
            c += DB.getInt(ky,0);
        SharedPreferences.Editor editor = DB.edit();
        editor.putInt(ky, c);
        editor.apply();
        return c;
    }

    public void pushBoolean(String key, boolean b) {
        SharedPreferences.Editor editor = DB.edit();
        editor.putBoolean(key, b);
        editor.apply();
    }
    public boolean getBoolean(String key) {
        if(DB.contains(key)) {
            return DB.getBoolean(key, false);
        }
        return false;
    }

    public LatLng getLastLocation() {
        String strLat = getString(PREF_GPS_LATITUDE);
        String strLng = getString(PREF_GPS_LONGITUDE);
        if(strLat.length()==0||strLng.length()==0)return new LatLng(0,0);
        double lat = Double.parseDouble(strLat);
        double lng = Double.parseDouble(strLng);
        return new LatLng(lat,lng);
    }
    public void setLastLocation(LatLng ll) {
        saveString(PREF_GPS_LATITUDE,String.valueOf(ll.latitude));
        saveString(PREF_GPS_LONGITUDE,String.valueOf(ll.longitude));
    }
}