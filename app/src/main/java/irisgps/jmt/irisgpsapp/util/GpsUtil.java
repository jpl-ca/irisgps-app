package irisgps.jmt.irisgpsapp.util;

import android.content.Context;

import irisgps.jmt.irisgpsapp.service.gps.GpsService;
import irisgps.jmt.irisgpsapp.service.gps.LocationFusedService;

/**
 * Created by JMTech-Android on 14/12/2015.
 */
public class GpsUtil {
    private static Class ClasicGpsService = GpsService.class;
    private static Class FusedGpsService = LocationFusedService.class;

    public static Class getGpsService(Context context) {
        int opc = 1;
        if(opc == 0){
            System.out.println("Iniciando-> GpsService.class");
            return ClasicGpsService;
        }else{
            System.out.println("Iniciando-> LocationFusedService.class");
            return FusedGpsService;
        }
    }
}