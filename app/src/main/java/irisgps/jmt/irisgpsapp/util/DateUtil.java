package irisgps.jmt.irisgpsapp.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by JMTech-Android on 14/12/2015.
 */
public class DateUtil {
    public static Date parseStrToDateTime(String dt) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(dt);
            return date;
        } catch (ParseException e) {
            return new Date();
        }
    }
}