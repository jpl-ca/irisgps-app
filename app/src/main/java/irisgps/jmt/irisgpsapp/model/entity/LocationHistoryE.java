package irisgps.jmt.irisgpsapp.model.entity;

/**
 * Created by JMTech-Android on 25/05/2015.
 */
public class LocationHistoryE{
    private long task_id;
    private double lat;
    private double lng;

    public LocationHistoryE(){}
    public LocationHistoryE(double lat, double lng){
        this.lat = lat;
        this.lng = lng;
    }
    public LocationHistoryE(long task_id,double lat, double lng){
        this.task_id = task_id;
        this.lat = lat;
        this.lng = lng;
    }

    public long getTask_id() {
        return task_id;
    }

    public void setTask_id(long task_id) {
        this.task_id = task_id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}