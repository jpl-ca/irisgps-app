package irisgps.jmt.irisgpsapp.model;

/**
 * Created by JMTech-Android on 22/01/2016.
 */
public final class S {
    public static final class VAR{
        public static final String SERVER_URL = "http://192.168.1.103";
        public static final String PREF_TOKEN = "Bearer";
        public static final String TOKEN = "Token";
        public static final String COOKIE = "Cookie";
        public static final String Accept = "application/json";
    }

    public static final class ENTITY{
        public static final String Agent = "agent";
        public static final String Vehicle = "vehicle";
        public static final String Type = "Type";
    }

    public static final class GCM{
        public static final String gcm_id= "gcm_id";
        public static final String new_gcm_id= "new_gcm_id";
        public static final String SENDER_ID= "313719429966";
    }

    public static final class ALERTA{
        public static final int ALERTA_INVISIBLE= 100;
        public static final int NUEVA_ALERTA= 101;
    }

    public static final class VISIT_STATE{
        public static final int Scheduled = 1;
        public static final int Done = 2;
        public static final int ReScheduling = 3;
        public static final int Cancelled = 4;
    }
    public static final class CHECKEDLIST_STATE{
        public static final int Unchecked= 0;
        public static final int Checked = 1;
    }
}