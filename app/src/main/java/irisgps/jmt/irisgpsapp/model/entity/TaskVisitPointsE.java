package irisgps.jmt.irisgpsapp.model.entity;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by JMTech-Android on 25/05/2015.
 */
public class TaskVisitPointsE {
    @SerializedName("id")
    private long task_visit_id;
    @SerializedName("checklist")
    private ArrayList<CheckListE> checkList;
    private String name;
    private double lat;
    private double lng;
    private String address;
    private String phone;
    private String datetime;
    private String time;
    private String reference;
    private int checklist_items;
    private int checklist_done_items;
    @SerializedName("visit_state_id")
    private int visitState;

    public long getTask_visit_id() {
        return task_visit_id;
    }

    public void setTask_visit_id(long task_visit_id) {
        this.task_visit_id = task_visit_id;
    }

    public ArrayList<CheckListE> getCheckList() {
        return checkList;
    }

    public void setCheckList(ArrayList<CheckListE> checkListEs) {
        this.checkList = checkListEs;
    }

    public int getVisitState() {
        return visitState;
    }

    public void setVisitState(int visitState) {
        this.visitState = visitState;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getLatLng() {
        return new LatLng(lat,lng);
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public int getChecklist_items() {
        return checklist_items;
    }

    public void setChecklist_items(int checklist_items) {
        this.checklist_items = checklist_items;
    }

    public int getChecklist_done_items() {
        return checklist_done_items;
    }

    public void setChecklist_done_items(int checklist_done_items) {
        this.checklist_done_items = checklist_done_items;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}