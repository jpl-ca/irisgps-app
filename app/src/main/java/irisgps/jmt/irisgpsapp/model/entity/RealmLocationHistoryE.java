package irisgps.jmt.irisgpsapp.model.entity;

import io.realm.RealmObject;

/**
 * Created by JMTech-Android on 25/05/2015.
 */
public class RealmLocationHistoryE extends RealmObject{
    private long task_id;
    private double lat;
    private double lng;

    public RealmLocationHistoryE(){}

    public long getTask_id() {
        return task_id;
    }

    public void setTask_id(long task_id) {
        this.task_id = task_id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}