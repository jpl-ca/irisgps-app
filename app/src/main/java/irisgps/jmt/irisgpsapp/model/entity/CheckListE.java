package irisgps.jmt.irisgpsapp.model.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by JMTech-Android on 25/05/2015.
 */
public class CheckListE {
    @SerializedName("id")
    private long checklist_id;
    private String description;
    private int checked;

    public long getChecklist_id() {
        return checklist_id;
    }

    public void setChecklist_id(long checklist_id) {
        this.checklist_id = checklist_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }
}