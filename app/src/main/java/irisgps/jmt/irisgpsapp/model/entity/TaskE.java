package irisgps.jmt.irisgpsapp.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by JMTech-Android on 25/05/2015.
 */
public class TaskE {
    @SerializedName("id")
    private long task_id;
    @SerializedName("visit_points")
    private ArrayList<TaskVisitPointsE> taskVisitPoints;
    private String description;
    private String start_date;
    private String end_date;

    public long getTask_id() {
        return task_id;
    }

    public void setTask_id(long task_id) {
        this.task_id = task_id;
    }

    public ArrayList<TaskVisitPointsE> getTaskVisitPoints() {
        return taskVisitPoints;
    }

    public void setTaskVisitPoints(ArrayList<TaskVisitPointsE> taskVisitPoints) {
        this.taskVisitPoints = taskVisitPoints;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }
}