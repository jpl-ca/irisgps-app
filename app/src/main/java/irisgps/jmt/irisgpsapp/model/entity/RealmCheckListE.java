package irisgps.jmt.irisgpsapp.model.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by JMTech-Android on 25/05/2015.
 */
public class RealmCheckListE extends RealmObject {
    @PrimaryKey
    private long checklist_id;
    private String description;
    private int checked;

    public long getChecklist_id() {
        return checklist_id;
    }

    public void setChecklist_id(long checklist_id) {
        this.checklist_id = checklist_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }
}