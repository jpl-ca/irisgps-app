package irisgps.jmt.irisgpsapp.model.entity;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by JMTech-Android on 25/05/2015.
 */
public class RealmTaskVisitPointsE extends RealmObject {
    @PrimaryKey
    private long task_visit_id;
    private RealmList<RealmCheckListE> checkList;
    private String name;
    private double lat;
    private double lng;
    private String address;
    private String phone;
    private String reference;
    private int visitState;
    private int checklist_items;
    private int checklist_done_items;

    public long getTask_visit_id() {
        return task_visit_id;
    }

    public void setTask_visit_id(long task_visit_id) {
        this.task_visit_id = task_visit_id;
    }

    public RealmList<RealmCheckListE> getCheckList() {
        return checkList;
    }

    public void setCheckList(RealmList<RealmCheckListE> checkList) {
        this.checkList = checkList;
    }

    public int getVisitState() {
        return visitState;
    }

    public void setVisitState(int visitState) {
        this.visitState = visitState;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public int getChecklist_items() {
        return checklist_items;
    }

    public void setChecklist_items(int checklist_items) {
        this.checklist_items = checklist_items;
    }

    public int getChecklist_done_items() {
        return checklist_done_items;
    }

    public void setChecklist_done_items(int checklist_done_items) {
        this.checklist_done_items = checklist_done_items;
    }
}