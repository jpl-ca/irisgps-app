package irisgps.jmt.irisgpsapp.service.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Date;

import irisgps.jmt.irisgpsapp.util.JMStore;
import irisgps.jmt.irisgpsapp.util.NetworkUtil;

/**
 * Created by JMTech-Android on 22/01/2016.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
    private Date date = new Date();
    private JMStore jmstore;
    private long max_time = 15*1000;

    @Override
    public void onReceive(Context context, Intent intent) {
        jmstore = new JMStore(context);
        long now_time = date.getTime();
        boolean allow = (now_time - jmstore.getLastTimeNetwork()) > max_time;
        jmstore.pushLastTimeNetwork(now_time);
        if(!allow)return;
        int status = NetworkUtil.getConnectivityStatus(context);
        jmstore.pushStatusNetwork(status);
    }
}