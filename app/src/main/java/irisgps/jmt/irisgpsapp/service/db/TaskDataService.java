package irisgps.jmt.irisgpsapp.service.db;

import io.realm.Realm;
import io.realm.RealmList;
import java.util.ArrayList;
import android.content.Context;

import io.realm.RealmResults;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.CheckListE;
import irisgps.jmt.irisgpsapp.model.entity.RealmCheckListE;
import irisgps.jmt.irisgpsapp.model.entity.TaskE;
import irisgps.jmt.irisgpsapp.model.entity.RealmTaskE;
import irisgps.jmt.irisgpsapp.model.entity.RealmTaskVisitPointsE;
import irisgps.jmt.irisgpsapp.service.db.db_interface.TaskDataServiceI;
import irisgps.jmt.irisgpsapp.model.rx.RealmObservable;
import irisgps.jmt.irisgpsapp.model.entity.TaskVisitPointsE;
import rx.functions.Func1;
import rx.Observable;

/**
 * Created by JMTech-Android on 26/01/2016.
 */
public class TaskDataService implements TaskDataServiceI{
    private final Context context;

    public TaskDataService(Context context){
        this.context = context;
    }

    @Override
    public Observable<TaskE> getTask() {
        return RealmObservable.object(context, new Func1<Realm, RealmTaskE>() {
            @Override
            public RealmTaskE call(Realm realm) {
                RealmTaskE realmTask = realm.where(RealmTaskE.class).findFirst();
                if(realmTask==null||realmTask.getTask_id()==0)throw new NullPointerException("There's not TaskE");
                return realmTask;
            }
        }).map(new Func1<RealmTaskE, TaskE>() {
            @Override
            public TaskE call(RealmTaskE rTask) {
                /**
                 * Parse RealmTaskE to TaskE
                 */
                return parseTaskFromRealm(rTask);
            }
        });
    }

    @Override
    public Observable<TaskE> registerTask(final TaskE task) {
        /**
         * Parse TaskE to RealmTaskE
         */
        return RealmObservable.object(context, new Func1<Realm, RealmTaskE>() {
            @Override
            public RealmTaskE call(Realm realm) {
                return realm.copyToRealmOrUpdate(parseTaskToRealm(task));
            }
        }).map(new Func1<RealmTaskE, TaskE>() {
            @Override
            public TaskE call(RealmTaskE realmIssue) {
                return task;
            }
        });
    }

    public Observable<CheckListE> checkListChecked(final long checklist_id) {
        return RealmObservable.object(context, new Func1<Realm, RealmCheckListE>() {
            @Override
            public RealmCheckListE call(Realm realm) {
                RealmResults<RealmCheckListE> allTV = realm.where(RealmCheckListE.class).findAll();
                RealmCheckListE checklist = new RealmCheckListE();
                for (RealmCheckListE tv: allTV) {
                    if(tv.getChecklist_id()==checklist_id)checklist = tv;
                }
                checklist.setChecked(S.CHECKEDLIST_STATE.Checked);
                return realm.copyToRealmOrUpdate(checklist);
            }
        }).map(new Func1<RealmCheckListE, CheckListE>() {
            @Override
            public CheckListE call(RealmCheckListE tvR) {
                return parseCheckListFromRealm(tvR);
            }
        });
    }
    public Observable<CheckListE> uncheckListChecked(final long checklist_id) {
        return RealmObservable.object(context, new Func1<Realm, RealmCheckListE>() {
            @Override
            public RealmCheckListE call(Realm realm) {
                RealmResults<RealmCheckListE> allTV = realm.where(RealmCheckListE.class).findAll();
                RealmCheckListE checklist = new RealmCheckListE();
                for (RealmCheckListE tv: allTV) {
                    if(tv.getChecklist_id()==checklist_id)checklist = tv;
                }
                checklist.setChecked(S.CHECKEDLIST_STATE.Unchecked);
                return realm.copyToRealmOrUpdate(checklist);
            }
        }).map(new Func1<RealmCheckListE, CheckListE>() {
            @Override
            public CheckListE call(RealmCheckListE tvR) {
                return parseCheckListFromRealm(tvR);
            }
        });
    }

    public Observable<TaskVisitPointsE> setTaskVisitPointsState(final long visit_point_id,final int new_state) {
        return RealmObservable.object(context, new Func1<Realm, RealmTaskVisitPointsE>() {
            @Override
            public RealmTaskVisitPointsE call(Realm realm) {
                RealmResults<RealmTaskVisitPointsE> allTV = realm.where(RealmTaskVisitPointsE.class).findAll();
                RealmTaskVisitPointsE checklist = new RealmTaskVisitPointsE();
                for (RealmTaskVisitPointsE tv: allTV) {
                    if(tv.getTask_visit_id()==visit_point_id)checklist = tv;
                }
                checklist.setVisitState(new_state);
                return realm.copyToRealmOrUpdate(checklist);
            }
        }).map(new Func1<RealmTaskVisitPointsE, TaskVisitPointsE>() {
            @Override
            public TaskVisitPointsE call(RealmTaskVisitPointsE tvR) {
                return parseTaskVisitFromRealm(tvR);
            }
        });
    }

    @Override
    public Observable<Boolean> removeTask() {
        return RealmObservable.object(context, new Func1<Realm, RealmTaskE>() {
            @Override
            public RealmTaskE call(Realm realm) {
                RealmTaskE us = realm.where(RealmTaskE.class).findFirst();
                realm.clear(RealmTaskE.class);
                System.out.println("Eliminando las tareas");
                return us;
            }
        }).map(new Func1<RealmTaskE, Boolean>() {
            @Override
            public Boolean call(RealmTaskE realmUser) {
                return true;
            }
        });
    }


    /**
     * @return RealmCheckList from CheckList
     */
    private RealmCheckListE parseCheckListToRealm(CheckListE chckList){
        RealmCheckListE chckListR = new RealmCheckListE();
        chckListR.setChecklist_id(chckList.getChecklist_id());
        chckListR.setChecked(chckList.getChecked());
        chckListR.setDescription(chckList.getDescription());
        return chckListR;
    }

    /**
     * @return CheckList from RealmCheckList
     */
    private CheckListE parseCheckListFromRealm(RealmCheckListE chckListR){
        CheckListE chckList = new CheckListE();
        chckList.setChecklist_id(chckListR.getChecklist_id());
        chckList.setChecked(chckListR.getChecked());
        chckList.setDescription(chckListR.getDescription());
        return chckList;
    }

    /**
     * @return TaskVisit from RealmTastVisit
     */
    private TaskVisitPointsE parseTaskVisitFromRealm(RealmTaskVisitPointsE rTask){
        TaskVisitPointsE tv = new TaskVisitPointsE();
        tv.setTask_visit_id(rTask.getTask_visit_id());
        tv.setLat(rTask.getLat());
        tv.setLng(rTask.getLng());
        tv.setAddress(rTask.getAddress());
        tv.setName(rTask.getName());
        tv.setPhone(rTask.getPhone());
        tv.setReference(rTask.getReference());
        tv.setVisitState(rTask.getVisitState());

        RealmList<RealmCheckListE> checkListR = rTask.getCheckList();
        ArrayList<CheckListE> checkList = new ArrayList<>();
        for (RealmCheckListE chckListR : checkListR){
            CheckListE chckList = parseCheckListFromRealm(chckListR);
            checkList.add(chckList);
        }
        tv.setCheckList(checkList);
        return tv;
    }

    /**
     * @return RealmTaskVisitE from TaskVisitE
     */
    private RealmTaskVisitPointsE parseTaskVisitToRealm(TaskVisitPointsE tv){
        RealmTaskVisitPointsE tvR = new RealmTaskVisitPointsE();
        tvR.setTask_visit_id(tv.getTask_visit_id());
        tvR.setLat(tv.getLat());
        tvR.setLng(tv.getLng());
        tvR.setAddress(tv.getAddress());
        tvR.setName(tv.getName());
        tvR.setPhone(tv.getPhone());
        tvR.setReference(tv.getReference());
        tvR.setVisitState(tv.getVisitState());

        ArrayList<CheckListE> checkList = tv.getCheckList();
        RealmList<RealmCheckListE> checkListR = new RealmList<>();
        for (CheckListE chckList : checkList){
            RealmCheckListE chckListR = parseCheckListToRealm(chckList);
            checkListR.add(chckListR);
        }
        tvR.setCheckList(checkListR);
        return tvR;
    }

    /**
     * @return RealmTaskE from TaskE
     */
    private RealmTaskE parseTaskToRealm(TaskE task){
        final RealmTaskE realmTask = new RealmTaskE();
        realmTask.setTask_id(task.getTask_id());
        realmTask.setDescription(task.getDescription());
        realmTask.setStart_date(task.getStart_date());
        realmTask.setEnd_date(task.getEnd_date());

        ArrayList<TaskVisitPointsE> taskVisits = task.getTaskVisitPoints();
        RealmList<RealmTaskVisitPointsE> taskVisitsRealm = new RealmList<>();
        for (TaskVisitPointsE tv: taskVisits){
            RealmTaskVisitPointsE tvR = parseTaskVisitToRealm(tv);
            taskVisitsRealm.add(tvR);
        }
        realmTask.setTaskVisitPoints(taskVisitsRealm);
        return realmTask;
    }

    /**
     * @return TaskE from RealmTaskE
     */
    private TaskE parseTaskFromRealm(RealmTaskE rTask){
        TaskE task = new TaskE();
        task.setTask_id(rTask.getTask_id());
        task.setDescription(rTask.getDescription());
        task.setStart_date(rTask.getStart_date());
        task.setEnd_date(rTask.getEnd_date());

        ArrayList<TaskVisitPointsE> taskVisits = new ArrayList<>();
        RealmList<RealmTaskVisitPointsE> taskVisitsRealm = rTask.getTaskVisitPoints();
        for (RealmTaskVisitPointsE tvR: taskVisitsRealm){
            TaskVisitPointsE tv = parseTaskVisitFromRealm(tvR);
            taskVisits.add(tv);
        }
        task.setTaskVisitPoints(taskVisits);
        return task;
    }
}