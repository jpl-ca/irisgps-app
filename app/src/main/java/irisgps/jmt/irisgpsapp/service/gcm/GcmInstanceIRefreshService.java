package irisgps.jmt.irisgpsapp.service.gcm;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
public class GcmInstanceIRefreshService extends InstanceIDListenerService {
    @Override
    public void onTokenRefresh() {
        System.out.println("onTokenRefresh()()()() Receiving update!!!");
        Intent intent = new Intent(this, GcmRegistrationIdService.class);
        startService(intent);
    }
}