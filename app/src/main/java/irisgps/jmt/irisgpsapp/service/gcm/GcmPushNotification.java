package irisgps.jmt.irisgpsapp.service.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.view.activity.SplashActivity;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
public class GcmPushNotification extends GcmListenerService {
    private int NOTIFICATION_NEWALERT = 101;
    @Override
    public void onMessageReceived(String from, Bundle data) {
        System.out.println("--------------------->");
        System.out.println("From es::::"+from);
        System.out.println(data.toString());
        if (from.startsWith("/topics/")) {
            System.out.println("message received from some topic.");
        } else {
            System.out.println("normal downstream message.");
        }
        try {
            int seci_notification_type = Integer.parseInt(data.getString("gcm.notification.seci_notification_type"));
            if(seci_notification_type==S.ALERTA.NUEVA_ALERTA){
                JSONObject citizen_=  new JSONObject(data.getString("data_data"));
                String titulo = "Titulo";
                String mensaje = "Mensaje";
                sendNotification(titulo,mensaje);
            }else if(seci_notification_type==S.ALERTA.ALERTA_INVISIBLE){
                cancelNotification();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    ////Todo  test del GSM here.....
    ////Todo  test del GSM here.....
    ////Todo  test del GSM here.....

    private void cancelNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_NEWALERT);
    }

    private void sendNotification(String titulo,String mensaje) {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(titulo)
                .setContentText(mensaje)
                .setTicker(titulo)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_NEWALERT /* ID of notification */, notificationBuilder.build());
    }
}