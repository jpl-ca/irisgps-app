package irisgps.jmt.irisgpsapp.service.http;

import android.content.Context;

import org.json.JSONObject;

import irisgps.jmt.irisgpsapp.model.entity.TrackeableE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import rx.Observable;

/**
 * Created by JMTech-Android on 10/12/2015.
 */
public class AccessAccountService extends ApiService{
    private LoginApi apiService;

    public AccessAccountService(Context context) {
        super(context);
        apiService = retrofit.create(LoginApi.class);
    }

    public LoginApi getApi() {
        return apiService;
    }

    public interface LoginApi{
//        @GET("sgtareas/api/auth/me")
        @GET("sgtareas_mirror/api/auth/me")
        Observable<TrackeableE> getSessionMe();

        @FormUrlEncoded
//        @POST("sgtareas/api/auth/login")
        @POST("sgtareas_mirror/api/auth/login")
        Observable<TrackeableE> userLoginVehicle(@Field("plate") String plate, @Field("password") String password);

        @FormUrlEncoded
//        @POST("sgtareas/api/auth/login")
        @POST("sgtareas_mirror/api/auth/login")
        Observable<TrackeableE> userLoginAgent(@Field("authentication_code") String authentication_code, @Field("password") String password);

        @FormUrlEncoded
//        @POST("sgtareas/api/organization/validate")
        @POST("sgtareas_mirror/api/organization/validate")
        Observable<Boolean> validateOrganization(@Field("name") String organization_name);

        @FormUrlEncoded
//        @POST("sgtareas/api/auth/me/update-notification-id")
        @POST("sgtareas_mirror/api/auth/me/update-notification-id")
        Observable<JSONObject> updateNotificationId(@Field("notification_id") String notification_id);

//        @GET("sgtareas/api/auth/logout")
        @GET("sgtareas_mirror/api/auth/logout")
        Observable<JSONObject> logout();
    }
}