package irisgps.jmt.irisgpsapp.service.http;

import android.content.Context;

import org.json.JSONObject;

import irisgps.jmt.irisgpsapp.model.entity.TaskE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import rx.Observable;

/**
 * Created by JMTech-Android on 10/12/2015.
 */
public class GeolocationService extends ApiService{
    private GeolocationApi apiService;

    public GeolocationService(Context context) {
        super(context);
        apiService = retrofit.create(GeolocationApi.class);
    }

    public GeolocationApi getApi() {
        return apiService;
    }

    public interface GeolocationApi{
        @FormUrlEncoded
//        @POST("sgtareas/api/auth/me/store-geolocation")
        @POST("sgtareas_mirror/api/auth/me/store-geolocation")
        Observable<JSONObject> storeGeolocation(@Field("lat") double lat,@Field("lng") double lng);

        @FormUrlEncoded
//        @POST("sgtareas/api/auth/me/store-geolocation")
        @POST("sgtareas_mirror/api/auth/me/store-geolocation")
        Observable<JSONObject> storeGeolocation(@Field("task_id") long task_id,@Field("lat") double lat,@Field("lng") double lng);

//        @GET("sgtareas/api/auth/me/tasks")
        @GET("sgtareas_mirror/api/auth/me/tasks")
        Observable<TaskE> getCurrentTask();

        @FormUrlEncoded
//        @POST("sgtareas/api/auth/me/check-list-item")
        @POST("sgtareas_mirror/api/auth/me/check-list-item")
        Observable<JSONObject> checkListCheck(@Field("id") long checklist_id);

        @FormUrlEncoded
//        @POST("sgtareas/api/auth/me/uncheck-list-item")
        @POST("sgtareas_mirror/api/auth/me/uncheck-list-item")
        Observable<JSONObject> uncheckListCheck(@Field("id") long checklist_id);

        @FormUrlEncoded
//        @POST("sgtareas/api/auth/me/reschedule-visit-point")
        @POST("sgtareas_mirror/api/auth/me/reschedule-visit-point")
        Observable<JSONObject> rescheduleVisitPoints(@Field("id") long visit_point_id,@Field("comment_of_rescheduling") String comment_of_rescheduling);
    }
}