package irisgps.jmt.irisgpsapp.service.db.db_interface;

import irisgps.jmt.irisgpsapp.model.entity.TrackeableE;
import rx.Observable;

/**
 * Created by JMTech-Android on 26/01/2016.
 */
public interface AgentDataServiceI {
    Observable<TrackeableE> getAgent();
    Observable<TrackeableE> registerAgent(TrackeableE user);
    Observable<Boolean> removeAgent();
}