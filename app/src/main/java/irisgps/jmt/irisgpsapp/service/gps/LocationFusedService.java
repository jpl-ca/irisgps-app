package irisgps.jmt.irisgpsapp.service.gps;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import irisgps.jmt.irisgpsapp.presenter.GpsLocationPresenter;
import irisgps.jmt.irisgpsapp.view.interface_view.GeolocationView;

/**
 * Created by JMTech-Android on 09/06/2015.
 */
    public class LocationFusedService extends Service implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener, GeolocationView {
    public static long UPDATE_INTERVAL_IN_MILLISECONDS = 60*1000;
    public static long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected Location mCurrentLocation;
    protected GpsLocationPresenter gpsLocationPresenter;
    public final static String NEW_GPS = "NEW_GPS";
    protected Boolean mRequestingLocationUpdates;
    private boolean hasFirstLocation = false;
    Intent intent;

    @Override
    public void onCreate() {
        super.onCreate();
        mRequestingLocationUpdates = false;
        gpsLocationPresenter = new GpsLocationPresenter(this);
        intent = new Intent();
        UPDATE_INTERVAL_IN_MILLISECONDS = gpsLocationPresenter.getIntervalUpdateLocation();
        FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS/2;
        buildGoogleApiClient();
        mGoogleApiClient.connect();
        System.out.println("geoAAAAAAAAAAAAA");
        gpsLocationPresenter = new GpsLocationPresenter(this);
        gpsLocationPresenter.waitForSomeSeconds();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build();
        createLocationRequest();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("geoBBBBBBBBBBB");
        System.out.println("creo que va a necesitar Iniciar geolocalizacion...");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mGoogleApiClient.isConnected()) {
                    System.out.println("Iniciar geolocalizacion...");
                    startLocationUpdates();
                    mRequestingLocationUpdates = true;
                }else
                    System.out.println("No se inicio geolocalizacion...");
            }
        }, 1500);
        return super.onStartCommand(intent, flags, startId);
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        hasFirstLocation = true;
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        System.out.println("Location was retrieved at :::>>>" + df.format(new Date(location.getTime())) + " :::" + location.getProvider() + " :::" + location.getAccuracy() + "  :::" + location.getSpeed());
        if(location.getAccuracy()<100){
            boolean saved = gpsLocationPresenter.registerGeoLocation(location);
            if(!saved){
                stopLocationUpdates();
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onDestroy() {
        System.out.println("-> GPS WAS SHUT DOWN!");
        System.out.println("-> GPS WAS SHUT DOWN!" + mRequestingLocationUpdates);
//        if (mRequestingLocationUpdates) {
            mRequestingLocationUpdates = false;
            stopLocationUpdates();
//        }
        super.onDestroy();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void stopService() {
        stopSelf();
    }

    @Override
    public boolean hasFirstLocation() {
        return hasFirstLocation;
    }

}