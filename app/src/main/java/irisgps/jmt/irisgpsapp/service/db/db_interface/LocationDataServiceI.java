package irisgps.jmt.irisgpsapp.service.db.db_interface;

import java.util.ArrayList;

import irisgps.jmt.irisgpsapp.model.entity.LocationHistoryE;
import rx.Observable;

/**
 * Created by JMTech-Android on 26/01/2016.
 */
public interface LocationDataServiceI {
    Observable<ArrayList<LocationHistoryE>> getLocations();
    Observable<LocationHistoryE> registerLocation(LocationHistoryE user);
    Observable<Boolean> removeLocations();
}