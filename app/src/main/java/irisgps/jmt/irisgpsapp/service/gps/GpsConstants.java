package irisgps.jmt.irisgpsapp.service.gps;

/**
 * Created by JMTech-Android on 22/01/2016.
 */
public class GpsConstants {
    /**
     * The global variables which hold the latitude and longitude as returned by the phone's GPS.
     */
    public static double GLOBAL_LATITUDE = 0;
    public static double GLOBAL_LONGITUDE = 0;
    public static double GLOBAL_LAST_LATITUDE = 0;
    public static double GLOBAL_LAST_LONGITUDE = 0;
    public static double GLOBAL_SPEED = 0;
    public static int TYPE_MESSAGE = 0;
    public static int TYPE_LOCATION = 10;
    public static int TYPE_DISCONNECT = 11;
}