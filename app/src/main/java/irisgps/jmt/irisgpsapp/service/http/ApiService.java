package irisgps.jmt.irisgpsapp.service.http;

import android.content.Context;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import io.realm.RealmObject;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.util.JMStore;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by JMTech-Android on 10/12/2015.
 */
public class ApiService {
    protected Retrofit retrofit;
    protected Context context;
    protected JMStore jmStore;

    public ApiService(Context context) {
        this.context = context;
        jmStore = new JMStore(context);
        Gson gson = new GsonBuilder().setExclusionStrategies(new  ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getDeclaringClass().equals(RealmObject.class);
            }
            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
            })
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient();
        client.setCookieHandler(new JMCookieManager(jmStore));
        client.interceptors().add(logging);
        client.interceptors().add(new InterceptorJM(context));
        retrofit = new Retrofit.Builder()
            .baseUrl(S.VAR.SERVER_URL)
            .client(client)
//            .client(new LoginServiceMock(context))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .build();
    }
}