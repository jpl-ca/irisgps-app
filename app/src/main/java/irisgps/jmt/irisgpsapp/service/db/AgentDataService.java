package irisgps.jmt.irisgpsapp.service.db;

import android.content.Context;

import io.realm.Realm;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.RealmAgentE;
import irisgps.jmt.irisgpsapp.model.entity.TrackeableE;
import irisgps.jmt.irisgpsapp.model.rx.RealmObservable;
import irisgps.jmt.irisgpsapp.service.db.db_interface.AgentDataServiceI;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by JMTech-Android on 26/01/2016.
 */
public class AgentDataService implements AgentDataServiceI{
    private final Context context;

    public AgentDataService(Context context){
        this.context = context;
    }

    @Override
    public Observable<TrackeableE> getAgent() {
        return RealmObservable.object(context, new Func1<Realm, RealmAgentE> () {
            @Override
            public RealmAgentE call(Realm realm) {
                RealmAgentE agentR = realm.where(RealmAgentE.class).findFirst();
                System.out.println("NEW AG::::"+agentR.getFirst_name());
                return agentR;
            }
        }).map(new Func1<RealmAgentE, TrackeableE>() {
            @Override
            public TrackeableE call(RealmAgentE realmUser) {
                TrackeableE user = new TrackeableE();
                user.setFirst_name(realmUser.getFirst_name());
                user.setLast_name(realmUser.getSurname());
                user.setType(S.ENTITY.Agent);
                return user;
            }
        });
    }

    @Override
    public Observable<TrackeableE> registerAgent(final TrackeableE user) {
        final RealmAgentE realmUser= new RealmAgentE();
        realmUser.setFirst_name(user.getFirst_name());
        realmUser.setSurname(user.getLast_name());
        return RealmObservable.object(context, new Func1<Realm, RealmAgentE>() {
            @Override
            public RealmAgentE call(Realm realm) {
                RealmAgentE agentR = realm.copyToRealmOrUpdate(realmUser);
                return agentR;
            }
        }).map(new Func1<RealmAgentE, TrackeableE>() {
            @Override
            public TrackeableE call(RealmAgentE realmIssue) {
                return user;
            }
        });
    }

    @Override
    public Observable<Boolean> removeAgent() {
        return RealmObservable.object(context, new Func1<Realm, RealmAgentE>() {
            @Override
            public RealmAgentE call(Realm realm) {
                RealmAgentE us = realm.where(RealmAgentE.class).findFirst();
                realm.where(RealmAgentE.class).findAll().clear();
                return us;
            }
        }).map(new Func1<RealmAgentE, Boolean>() {
            @Override
            public Boolean call(RealmAgentE realmUser) {
                return true;
            }
        });
    }

}