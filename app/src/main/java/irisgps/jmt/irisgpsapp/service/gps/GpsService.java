package irisgps.jmt.irisgpsapp.service.gps;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import irisgps.jmt.irisgpsapp.presenter.GpsLocationPresenter;
import irisgps.jmt.irisgpsapp.view.interface_view.GeolocationView;

/**
 * Created by JMTech-Android on 22/01/2016.
 */
public class GpsService extends Service implements LocationListener, GeolocationView {
    public static final String RECEIVE_GPS = "GPS.Receive.GPS";
    public static int INTERVAL_TIME_UPDATE;
    LocationManager locationManager;
    GpsLocationPresenter gpsLocationPresenter;
    private boolean hasFirstLocation = false;

    public GpsService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        gpsLocationPresenter = new GpsLocationPresenter(this);
        gpsLocationPresenter.waitForSomeSeconds();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        INTERVAL_TIME_UPDATE = gpsLocationPresenter.getIntervalUpdateLocation();
        System.out.println("*-*Asignando tiempo: " + INTERVAL_TIME_UPDATE);
        try {
            startGpsListener();

            Location location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            if (location.getAccuracy() < 100) {
                System.out.println("-----------------------------------------");
                System.out.println(location.getLatitude());
                System.out.println(location.getLongitude());
                System.out.println(location.getProvider());
                System.out.println(location.getAccuracy());
                System.out.println(".............");
                System.out.println(location.getSpeed());
                System.out.println(location.getTime());
                long nw = new Date().getTime();
                System.out.println(nw);
                System.out.println(nw - location.getTime());
                System.out.println("-----------------------------------------");
                gpsLocationPresenter.registerGeoLocation(location);
            } else {
                System.out.println("The Accuracy is bad: " + location.getAccuracy());
                System.out.println("The Location bad: " + location.getLatitude() + "," + location.getLongitude());
            }
        } catch (SecurityException e) {
            System.out.println("->Cannot locate device without explicit Location permissions!");
            e.printStackTrace();
        }
//        return Service.START_STICKY;
        return super.onStartCommand(intent, flags, startId);
    }

    private void startGpsListener() throws SecurityException {
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, INTERVAL_TIME_UPDATE, 1f, this);
        } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, INTERVAL_TIME_UPDATE, 1f, this);
        }

        gpsLocationPresenter.checkGpsIsUpdating();
    }

    private void removeUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if(locationManager != null){
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("-> GPS WAS SHUT DOWN!");
        removeUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        hasFirstLocation = true;
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        System.out.println("Location was retrieved at :::>>>" + df.format(new Date(location.getTime())) + " :::" + location.getProvider() + " :::" + location.getAccuracy() + "  :::" + location.getSpeed());
        if(location.getAccuracy()<100)
            gpsLocationPresenter.registerGeoLocation(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        System.out.println("Cambio de estado -> "+s+" -> "+i);
    }

    @Override
    public void onProviderEnabled(String provider) {
        System.out.println("xyzxyzxyzxyzxyzxyz");
        try {
            locationManager.requestLocationUpdates(provider, INTERVAL_TIME_UPDATE, 0, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProviderDisabled(String s) {
        System.out.println("123123123123121123");
        try {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, INTERVAL_TIME_UPDATE, 0, this);
        } catch (SecurityException e) {
            e.printStackTrace();
            System.out.println("->Switching to Network!");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            System.out.println("->Please turn on GPS!");
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void stopService() {
        stopSelf();
    }

    @Override
    public boolean hasFirstLocation() {
        return hasFirstLocation;
    }
}