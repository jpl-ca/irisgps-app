package irisgps.jmt.irisgpsapp.service.db;

import android.content.Context;

import io.realm.Realm;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.RealmVehicleE;
import irisgps.jmt.irisgpsapp.model.entity.TrackeableE;
import irisgps.jmt.irisgpsapp.model.rx.RealmObservable;
import irisgps.jmt.irisgpsapp.service.db.db_interface.VehicleDataServiceI;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by JMTech-Android on 26/01/2016.
 */
public class VehicleDataService implements VehicleDataServiceI{
    private final Context context;

    public VehicleDataService(Context context){
        this.context = context;
    }

    @Override
    public Observable<TrackeableE> getVehicle() {
        return RealmObservable.object(context, new Func1<Realm, RealmVehicleE> () {
            @Override
            public RealmVehicleE call(Realm realm) {
                return realm.where(RealmVehicleE.class).findFirst();
            }
        }).map(new Func1<RealmVehicleE, TrackeableE>() {
            @Override
            public TrackeableE call(RealmVehicleE realmUser) {
                TrackeableE user = new TrackeableE();
                user.setPlate(realmUser.getPlate());
                user.setBrand(realmUser.getBrand());
                user.setModel(realmUser.getModel());
                user.setColor(realmUser.getColor());
                user.setType(S.ENTITY.Vehicle);
                return user;
            }
        });
    }

    @Override
    public Observable<TrackeableE> registerVehicle(final TrackeableE user) {
        final RealmVehicleE realmUser= new RealmVehicleE();
        realmUser.setPlate(user.getPlate());
        realmUser.setBrand(user.getBrand());
        realmUser.setModel(user.getModel());
        realmUser.setColor(user.getColor());
        return RealmObservable.object(context, new Func1<Realm, RealmVehicleE>() {
            @Override
            public RealmVehicleE call(Realm realm) {
                return realm.copyToRealm(realmUser);
            }
        }).map(new Func1<RealmVehicleE, TrackeableE>() {
            @Override
            public TrackeableE call(RealmVehicleE realmIssue) {
                return user;
            }
        });
    }

    @Override
    public Observable<Boolean> removeVehicle() {
        return RealmObservable.object(context, new Func1<Realm, RealmVehicleE>() {
            @Override
            public RealmVehicleE call(Realm realm) {
                RealmVehicleE us = realm.where(RealmVehicleE.class).findFirst();
                realm.where(RealmVehicleE.class).findAll().clear();
                System.out.println("->"+us);
                return us;
            }
        }).map(new Func1<RealmVehicleE, Boolean>() {
            @Override
            public Boolean call(RealmVehicleE realmUser) {
                System.out.println("xyz");
                return true;
            }
        });
    }

}