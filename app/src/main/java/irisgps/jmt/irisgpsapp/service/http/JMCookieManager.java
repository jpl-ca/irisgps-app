package irisgps.jmt.irisgpsapp.service.http;

import java.io.IOException;
import java.net.CookieManager;
import java.net.URI;
import java.util.List;
import java.util.Map;

import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.util.JMStore;

/**
 * Created by JMTech-Android on 12/01/2016.
 */
public class JMCookieManager extends CookieManager {
    JMStore jmStore;
    public JMCookieManager(JMStore jmStore){
        this.jmStore = jmStore;
    }
    @Override
    public void put(URI uri, Map<String, List<String>> stringListMap) throws IOException {
        super.put(uri, stringListMap);
        if (stringListMap != null && stringListMap.get("Set-Cookie") != null)
            for (String strCK : stringListMap.get("Set-Cookie")) {
                jmStore.saveString(S.VAR.COOKIE, strCK);
            }
    }
}