package irisgps.jmt.irisgpsapp.service.db.db_interface;

import irisgps.jmt.irisgpsapp.model.entity.TrackeableE;
import rx.Observable;

/**
 * Created by JMTech-Android on 26/01/2016.
 */
public interface VehicleDataServiceI {
    Observable<TrackeableE> getVehicle();
    Observable<TrackeableE> registerVehicle(TrackeableE user);
    Observable<Boolean> removeVehicle();
}