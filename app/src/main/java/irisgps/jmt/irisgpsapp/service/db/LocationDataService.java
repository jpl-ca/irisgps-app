package irisgps.jmt.irisgpsapp.service.db;

import android.content.Context;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import irisgps.jmt.irisgpsapp.model.entity.LocationHistoryE;
import irisgps.jmt.irisgpsapp.model.entity.RealmLocationHistoryE;
import irisgps.jmt.irisgpsapp.model.rx.RealmObservable;
import irisgps.jmt.irisgpsapp.service.db.db_interface.LocationDataServiceI;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by JMTech-Android on 26/01/2016.
 */
public class LocationDataService implements LocationDataServiceI{
    private final Context context;

    public LocationDataService(Context context){
        this.context = context;
    }

    @Override
    public Observable<ArrayList<LocationHistoryE>> getLocations() {
        return RealmObservable.results(context, new Func1<Realm, RealmResults<RealmLocationHistoryE>>() {
            @Override
            public RealmResults<RealmLocationHistoryE> call(Realm realm) {
                return realm.where(RealmLocationHistoryE.class).findAll();
            }
        }).map(new Func1<RealmResults<RealmLocationHistoryE>, ArrayList<LocationHistoryE>>() {
            @Override
            public ArrayList<LocationHistoryE> call(RealmResults<RealmLocationHistoryE> realmLocation) {
                ArrayList<LocationHistoryE> locationsList = new ArrayList<>();
                for (RealmLocationHistoryE realmLoc:realmLocation){
                    LocationHistoryE loc = new LocationHistoryE();
                    loc.setTask_id(realmLoc.getTask_id());
                    loc.setLat(realmLoc.getLat());
                    loc.setLng(realmLoc.getLng());
                    locationsList.add(loc);
                }
                return locationsList;
            }
        });
    }

    @Override
    public Observable<LocationHistoryE> registerLocation(final LocationHistoryE location) {
        final RealmLocationHistoryE realmLocation = new RealmLocationHistoryE();
        realmLocation.setTask_id(location.getTask_id());
        realmLocation.setLat(location.getLat());
        realmLocation.setLng(location.getLng());
        return RealmObservable.object(context, new Func1<Realm, RealmLocationHistoryE>() {
            @Override
            public RealmLocationHistoryE call(Realm realm) {
                RealmLocationHistoryE locaR = realm.copyToRealm(realmLocation);
                return locaR;
            }
        }).map(new Func1<RealmLocationHistoryE, LocationHistoryE>() {
            @Override
            public LocationHistoryE call(RealmLocationHistoryE realmIssue) {
                return location;
            }
        });
    }

    @Override
    public Observable<Boolean> removeLocations() {
        return RealmObservable.object(context, new Func1<Realm, RealmLocationHistoryE>() {
            @Override
            public RealmLocationHistoryE call(Realm realm) {
                RealmLocationHistoryE us = realm.where(RealmLocationHistoryE.class).findFirst();
                realm.where(RealmLocationHistoryE.class).findAll().clear();
                return us;
            }
        }).map(new Func1<RealmLocationHistoryE, Boolean>() {
            @Override
            public Boolean call(RealmLocationHistoryE realmUser) {
                return true;
            }
        });
    }
}