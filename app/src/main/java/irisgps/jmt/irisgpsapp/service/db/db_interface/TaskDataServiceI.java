package irisgps.jmt.irisgpsapp.service.db.db_interface;

import irisgps.jmt.irisgpsapp.model.entity.TaskE;
import rx.Observable;

/**
 * Created by JMTech-Android on 26/01/2016.
 */
public interface TaskDataServiceI {
    Observable<TaskE> getTask();
    Observable<TaskE> registerTask(TaskE user);
    Observable<Boolean> removeTask();
}