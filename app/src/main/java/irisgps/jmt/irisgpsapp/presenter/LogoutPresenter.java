package irisgps.jmt.irisgpsapp.presenter;

import android.content.Context;

import org.json.JSONObject;

import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.service.db.AgentDataService;
import irisgps.jmt.irisgpsapp.service.db.TaskDataService;
import irisgps.jmt.irisgpsapp.service.db.VehicleDataService;
import irisgps.jmt.irisgpsapp.service.http.AccessAccountService;
import irisgps.jmt.irisgpsapp.util.JMStore;
import irisgps.jmt.irisgpsapp.view.interface_view.LogoutView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class LogoutPresenter {
    LogoutView delegate;
    private AgentDataService agentDataService;
    private VehicleDataService vehicleDataService;
    private CompositeSubscription compositeSubscription;
    private AccessAccountService accessAccountService;
    private JMStore jmStore;
    Context context;
    public LogoutPresenter(){}
    public LogoutPresenter(LogoutView delegate){
        this.delegate = delegate;
        setContext(delegate.getContext());
    }

    public void setContext(Context context){
        this.context = context;
        compositeSubscription = new CompositeSubscription();
        accessAccountService = new AccessAccountService(context);
        agentDataService = new AgentDataService(context);
        vehicleDataService = new VehicleDataService(context);
        jmStore = new JMStore(context);
    }

    public void finish(){
        compositeSubscription.unsubscribe();
    }

    public void logout() {
        jmStore.pushBoolean(JMStore.PREF_HAS_SESSION,false);
        Subscription subscription = accessAccountService.getApi().logout().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<JSONObject>() {
                            @Override
                            public void call(JSONObject str) {
                                removeAllData();
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                removeAllData();
                                throwable.printStackTrace();
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    public void removeAllData() {
        deleteTask();
        deleteUser();
    }
    private void deleteTask() {
        TaskDataService taskDataService = new TaskDataService(context);
        Subscription subscription = taskDataService.removeTask().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<Boolean>() {
                            @Override
                            public void call(Boolean b) {}
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {}
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    public void deleteUser() {
        Subscription subscription = null;
        System.out.println("abcdefgohijklmnopqrstuvwxyz out -->"+jmStore.getString(S.ENTITY.Type));
        if(jmStore.getString(S.ENTITY.Type).equals(S.ENTITY.Agent)){
            subscription = agentDataService.removeAgent().
                    subscribeOn(Schedulers.io()).
                    observeOn(AndroidSchedulers.mainThread()).
                    subscribe(
                            new Action1<Boolean>() {
                                @Override
                                public void call(Boolean b) {
                                    jmStore.saveString(S.VAR.TOKEN,"");
                                    System.out.println("----------------->Logout");
                                    if(delegate!=null)
                                        delegate.onLogoutSuccess();
                                }
                            },
                            new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    System.out.println("454564564644564546");
                                    throwable.printStackTrace();
                                    jmStore.saveString(S.VAR.TOKEN, "");
                                    if(delegate!=null)
                                        delegate.onLogoutSuccess();
                                }
                            }
                    );
        }else if(jmStore.getString(S.ENTITY.Type).equals(S.ENTITY.Vehicle)){
            System.out.println("Eliminando el veh...");
            subscription = vehicleDataService.removeVehicle().
                    subscribeOn(Schedulers.io()).
                    observeOn(AndroidSchedulers.mainThread()).
                    subscribe(
                            new Action1<Boolean>() {
                                @Override
                                public void call(Boolean b) {
                                    jmStore.saveString(S.VAR.TOKEN, "");
                                    System.out.println("----------------->Logout");
                                    if(delegate!=null)
                                        delegate.onLogoutSuccess();
                                }
                            },
                            new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    System.out.println("no se por que el error");
                                    throwable.printStackTrace();
                                    jmStore.saveString(S.VAR.TOKEN,"");
                                    if(delegate!=null)
                                        delegate.onLogoutSuccess();
                                }
                            }
                    );
        }
        if (compositeSubscription != null) {
            if(subscription!=null)
                compositeSubscription.add(subscription);
        }
    }
}