package irisgps.jmt.irisgpsapp.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import irisgps.jmt.irisgpsapp.view.dialog.LoadingDialog;
import irisgps.jmt.irisgpsapp.view.dialog.MessageDialog;
import irisgps.jmt.irisgpsapp.view.interface_view.CallbackMessageDialog;
import irisgps.jmt.irisgpsapp.view.interface_view.MessageDialogView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class MessageDialogPresenter {
    MessageDialogView delegate;
    private String url;
    private Gson gson;
    private MessageDialog messageDialog;

    public MessageDialogPresenter(MessageDialogView delegate) {
        this.delegate = delegate;
        messageDialog = new MessageDialog(delegate.getContext());
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
    }

    public void showDialog(String message) {
        messageDialog.showMessageDialog(message, new CallbackMessageDialog() {
            @Override
            public void finish() {
                delegate.clickOk();
            }
        });
    }
}