package irisgps.jmt.irisgpsapp.presenter;

import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.net.ConnectException;

import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.service.http.AccessAccountService;
import irisgps.jmt.irisgpsapp.util.JMStore;
import irisgps.jmt.irisgpsapp.view.interface_view.OrganizationView;
import retrofit.HttpException;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class OrganizationPresenter {
    OrganizationView delegate;
    private AccessAccountService accessAccountService;
    private CompositeSubscription compositeSubscription;
    public OrganizationPresenter(OrganizationView delegate){
        this.delegate = delegate;
        compositeSubscription = new CompositeSubscription();
        accessAccountService = new AccessAccountService(delegate.getContext());
    }

    public void finish(){
        compositeSubscription.unsubscribe();
    }

    public void validateOrganization(String organization_name){
        Subscription subscription = accessAccountService.getApi().validateOrganization(organization_name).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<Boolean>() {
                            @Override
                            public void call(Boolean b) {
                                saveOrganizationValidation(b);
                                delegate.isOrganization(b);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                ConnectException connEx;
                                HttpException httpEx;
                                throwable.printStackTrace();
                                if (throwable instanceof ConnectException) {
                                    connEx = ((ConnectException) throwable);
                                    System.out.println(delegate.getContext().getString(R.string.s_error_de_conexion)+" Posiblemente wifi esta desconectado o ip no existe");
                                    delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                } else if (throwable instanceof HttpException) {
                                    httpEx = ((HttpException) throwable);
                                    System.out.println(httpEx.code());
                                    ResponseBody responseBody = httpEx.response().errorBody();
                                    try {
                                        String str = responseBody.string();
                                        System.out.println("--->" + str);
                                        delegate.onRequestError(str);
                                    } catch (IOException e) {
                                        delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                    }
                                }
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    private void saveOrganizationValidation(boolean b) {
        new JMStore(delegate.getContext()).pushValidateOrganization(b);
    }
}