package irisgps.jmt.irisgpsapp.presenter;

import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.TrackeableE;
import irisgps.jmt.irisgpsapp.service.db.AgentDataService;
import irisgps.jmt.irisgpsapp.service.db.VehicleDataService;
import irisgps.jmt.irisgpsapp.service.http.AccessAccountService;
import irisgps.jmt.irisgpsapp.util.JMStore;
import irisgps.jmt.irisgpsapp.view.interface_view.UserAccountView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class UserAccountPresenter {
    UserAccountView delegate;
    private AgentDataService agentDataService;
    private VehicleDataService vehicleDataService;
    private CompositeSubscription compositeSubscription;
    private AccessAccountService accessAccountService;
    private JMStore jmStore;
    public UserAccountPresenter(UserAccountView delegate){
        this.delegate = delegate;
        compositeSubscription = new CompositeSubscription();
        accessAccountService = new AccessAccountService(delegate.getContext());
        agentDataService = new AgentDataService(delegate.getContext());
        vehicleDataService = new VehicleDataService(delegate.getContext());
        jmStore = new JMStore(delegate.getContext());
    }

    public void finish(){
        compositeSubscription.unsubscribe();
    }

    public void getProfileInfo() {
        Subscription subscription = null;
        System.out.println(jmStore.getString(S.ENTITY.Type));
        if(jmStore.getString(S.ENTITY.Type).equals(S.ENTITY.Agent)){
                subscription = agentDataService.getAgent().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<TrackeableE>() {
                            @Override
                            public void call(TrackeableE user) {
                                delegate.onRequestSuccess(user);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                                delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_base_de_datos));
                            }
                        }
                );
        }else if(jmStore.getString(S.ENTITY.Type).equals(S.ENTITY.Vehicle)){
                subscription = vehicleDataService.getVehicle().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<TrackeableE>() {
                            @Override
                            public void call(TrackeableE user) {
                                delegate.onRequestSuccess(user);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                                delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_base_de_datos));
                            }
                        }
                );
        }
        if (compositeSubscription != null) {
            if(subscription!=null)
                compositeSubscription.add(subscription);
        }
    }
}