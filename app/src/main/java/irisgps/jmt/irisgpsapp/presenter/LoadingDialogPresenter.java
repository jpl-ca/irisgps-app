package irisgps.jmt.irisgpsapp.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import irisgps.jmt.irisgpsapp.view.dialog.LoadingDialog;
import irisgps.jmt.irisgpsapp.view.interface_view.LoadingDialogView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class LoadingDialogPresenter {
    LoadingDialogView delegate;
    private String url;
    private Gson gson;
    private LoadingDialog loadingDialog;

    public LoadingDialogPresenter(LoadingDialogView delegate){
        this.delegate = delegate;
        loadingDialog = new LoadingDialog(delegate.getContext());
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
    }

    public void showDialog(String message){
        loadingDialog.showLoadingDialog(message);
    }

    public void hideDialog(){
        loadingDialog.hideDialog();
    }
}