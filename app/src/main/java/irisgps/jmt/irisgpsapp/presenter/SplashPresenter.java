package irisgps.jmt.irisgpsapp.presenter;

import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;

import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.TrackeableE;
import irisgps.jmt.irisgpsapp.service.http.AccessAccountService;
import irisgps.jmt.irisgpsapp.util.JMStore;
import irisgps.jmt.irisgpsapp.view.interface_view.SplashView;
import retrofit.HttpException;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class SplashPresenter {
    private SplashView delegate;
    private AccessAccountService accessAccountService;
    private CompositeSubscription compositeSubscription;
    JMStore jmStore;
    public SplashPresenter(SplashView delegate){
        this.delegate = delegate;
        compositeSubscription = new CompositeSubscription();
        accessAccountService = new AccessAccountService(delegate.getContext());
        jmStore = new JMStore(delegate.getContext());
    }

    public void finish(){
        compositeSubscription.unsubscribe();
    }

    public void getInfo(){
        if(!jmStore.getString(S.VAR.TOKEN).equals("")){
            getSessionMe();
        }else{
            delegate.onSessionError(hasOrganization(), delegate.getContext().getString(R.string.s_no_tiene_sesion));
        }
    }

    private void getSessionMe(){
        Observable<TrackeableE> sessionMe = accessAccountService.getApi().getSessionMe();

        Subscription subscription = accessAccountService.getApi().getSessionMe().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<TrackeableE>() {
                            @Override
                            public void call(TrackeableE track) {
                                System.out.println("-->"+track.getType());
                                if(track.getType().equals(S.ENTITY.Agent)){
                                }else if(track.getType().equals(S.ENTITY.Vehicle)){
                                }
                                saveIntervalTimeGps(track.getLocation_frequency());
                                System.out.println("El intervalo es:"+track.getLocation_frequency());
                                delegate.onSessionSuccess();
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                ConnectException connEx;
                                HttpException httpEx;
                                IllegalStateException gsonEx;
                                SocketTimeoutException stEx;
                                System.out.println("Error");
                                if(throwable instanceof HttpException) {
                                    httpEx = ((HttpException) throwable);
                                    if(httpEx.code()==401||httpEx.code()==403){
                                        System.out.println("Error con la sesion en el seridor, recomendable es borrar el token almacenado");
                                        jmStore.saveString(S.VAR.TOKEN, "");
                                    }
                                    System.out.println(httpEx.code());
                                    ResponseBody responseBody = httpEx.response().errorBody();
                                    try {
                                        String str = responseBody.string();
                                        System.out.println("--->" + str);
                                        delegate.onSessionError(hasOrganization(), str);
                                    } catch (IOException e) {
                                        delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                    }
                                }else if (throwable instanceof ConnectException) {
                                    connEx = ((ConnectException) throwable);
                                    System.out.println(delegate.getContext().getString(R.string.s_error_de_conexion));
                                    delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                }else if(throwable instanceof IllegalStateException) {
                                    gsonEx =  ((IllegalStateException) throwable);
                                    System.out.println("--->" + gsonEx.getMessage());
                                    delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                }else if(throwable instanceof SocketTimeoutException) {
                                    stEx =  ((SocketTimeoutException) throwable);
                                    System.out.println("--->" + stEx.getMessage());
                                    delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                }else{
                                    delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                    throwable.printStackTrace();
                                }
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    private void saveIntervalTimeGps(int i) {
        jmStore.pushIntervalTimeGps(i);
    }

    private boolean hasOrganization() {
        return jmStore.getValidateOrganization();
    }
}