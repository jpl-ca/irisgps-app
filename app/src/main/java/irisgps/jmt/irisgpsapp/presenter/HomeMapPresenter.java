package irisgps.jmt.irisgpsapp.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;

import com.google.android.gms.maps.model.LatLng;
import irisgps.jmt.irisgpsapp.service.gps.GpsConstants;
import irisgps.jmt.irisgpsapp.service.gps.GpsService;
import irisgps.jmt.irisgpsapp.util.JMStore;
import irisgps.jmt.irisgpsapp.view.interface_view.HomeMapView;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class HomeMapPresenter {
    private HomeMapView delegate;
    private Context ctx;
    private CompositeSubscription compositeSubscription;
    JMStore jmStore;
    private final double LAT_DEFAULT = -12.046374;
    private final double LNG_DEFAULT = -77.042793;
    public HomeMapPresenter(HomeMapView delegate){
        this.delegate = delegate;
        ctx = delegate.getContext();
        jmStore = new JMStore(ctx);
        compositeSubscription = new CompositeSubscription();
    }

    public LatLng getLastLocation(){
        LatLng ll = jmStore.getLastLocation();
        if(ll.latitude==0||ll.longitude==0) ll = new LatLng(LAT_DEFAULT,LNG_DEFAULT);
        sendLastLocation(ll);
        return ll;
    }

    private void sendLastLocation(final LatLng ll) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                delegate.updatePosition(ll);
            }
        }, 1000);
    }

    public void finish(){
        compositeSubscription.unsubscribe();
    }

    public void startGeoLocation() {
        System.out.println("ctx.registerReceiver(mGPSReceiver, new IntentFilter(GpsService.RECEIVE_GPS));");
        ctx.registerReceiver(mGPSReceiver, new IntentFilter(GpsService.RECEIVE_GPS));
    }

    public void stoptGeoLocation() {
        try {
            System.out.println("ctx.unregisterReceiver(mGPSReceiver)");
            ctx.unregisterReceiver(mGPSReceiver);
        } catch (IllegalArgumentException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver mGPSReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(GpsConstants.TYPE_MESSAGE == GpsConstants.TYPE_LOCATION) {
                if(GpsConstants.GLOBAL_LATITUDE != 0 && GpsConstants.GLOBAL_LONGITUDE != 0) {
                    LatLng ll = new LatLng(GpsConstants.GLOBAL_LATITUDE, GpsConstants.GLOBAL_LONGITUDE);
//                    delegate.updatePosition(ll);
                } else {
                    System.out.println("-> No hay localizacion");
                }
            }else if(GpsConstants.TYPE_MESSAGE == GpsConstants.TYPE_DISCONNECT) {
                delegate.withoutSession();
            }
        }
    };
}