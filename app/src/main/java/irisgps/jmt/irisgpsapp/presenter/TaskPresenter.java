package irisgps.jmt.irisgpsapp.presenter;

import android.app.PendingIntent;
import android.os.Handler;

import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.net.ConnectException;
import java.util.Date;

import io.realm.exceptions.RealmException;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.entity.TaskE;
import irisgps.jmt.irisgpsapp.service.db.TaskDataService;
import irisgps.jmt.irisgpsapp.service.http.GeolocationService;
import irisgps.jmt.irisgpsapp.util.DateUtil;
import irisgps.jmt.irisgpsapp.view.interface_view.TaskView;
import retrofit.HttpException;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class TaskPresenter {
    TaskView delegate;
    private GeolocationService geolocationService;
    private CompositeSubscription compositeSubscription;
    private TaskDataService taskDataService;
    private PendingIntent pendingIntent;
    Runnable taskRunnable;
    Handler handlerToList =  new Handler();
    public TaskPresenter(TaskView delegate){
        this.delegate = delegate;
        compositeSubscription = new CompositeSubscription();
        taskDataService = new TaskDataService(delegate.getContext());
        geolocationService = new GeolocationService(delegate.getContext());
    }

    public void finish(){
        compositeSubscription.unsubscribe();
    }
    public void finishAnyHandler(){
        if(taskRunnable!=null)
        handlerToList.removeCallbacks(taskRunnable);
    }

    public void loadCurrentTask(){
        Subscription subscription = geolocationService.getApi().getCurrentTask().
        subscribeOn(Schedulers.io()).
        observeOn(AndroidSchedulers.mainThread()).
        subscribe(
                new Action1<TaskE>() {
                    @Override
                    public void call(TaskE task) {
                        if (task == null) {
                            delegate.onRequestError(delegate.getContext().getString(R.string.s_no_tiene_tarea_asignada));
                        } else {
                            sendTaskList(task);
                            saveTaskLocal(task);
                        }
                    }
                },
                new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        ConnectException connEx;
                        HttpException httpEx;
                        throwable.printStackTrace();
                        if (throwable instanceof ConnectException) {
                            connEx = ((ConnectException) throwable);
                            System.out.println(delegate.getContext().getString(R.string.s_error_de_conexion));
                            delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                        } else if (throwable instanceof HttpException) {
                            httpEx = ((HttpException) throwable);
                            System.out.println(httpEx.code());
                            ResponseBody responseBody = httpEx.response().errorBody();
                            try {
                                String str = responseBody.string();
                                System.out.println("--->" + str);
                                delegate.onRequestError(str);
                            } catch (IOException e) {
                                delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                            }
                        }
                    }
                }
        );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    private void sendTaskList(final TaskE task) {
        Date start = DateUtil.parseStrToDateTime(task.getStart_date());
        Date end = DateUtil.parseStrToDateTime(task.getEnd_date());
        Date now = new Date();
        if(now.getTime()>=start.getTime()&&now.getTime()<=end.getTime())
            delegate.listTask(task);
        else if(now.getTime()<start.getTime()){
            delegate.onRequestError(delegate.getContext().getString(R.string.s_no_tiene_tarea_asignada_para_este_momento));
            taskRunnable = new Runnable() {
                @Override
                public void run() {
                    delegate.listTask(task);
                }
            };
            handlerToList.postDelayed(taskRunnable, start.getTime()-now.getTime());
        }
    }


    public void saveTaskLocal(TaskE task) {
        Subscription subscription = taskDataService.registerTask(task).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<TaskE>() {
                            @Override
                            public void call(TaskE task) {
                                System.out.println("Tarea fue registrado...");
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                System.out.println("Tarea no fue registrado...");
                                throwable.printStackTrace();
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    public void getCurrentTaskLocal() {
        Subscription subscription = taskDataService.getTask().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<TaskE>() {
                            @Override
                            public void call(TaskE task) {
                                Date start = DateUtil.parseStrToDateTime(task.getStart_date());
                                Date end = DateUtil.parseStrToDateTime(task.getEnd_date());
                                Date now = new Date();
                                if(now.getTime()>=start.getTime()&&now.getTime()<=end.getTime()){
                                    sendTaskList(task);
                                }else if(now.getTime()>end.getTime()){
                                    deleteTask();
                                    loadCurrentTask();
                                }
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                System.out.println("B----> BAD");
                                if (throwable instanceof RealmException) {
                                    loadCurrentTask();
                                }else{
                                    throwable.printStackTrace();
                                }
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    private void deleteTask() {
        TaskDataService taskDataService = new TaskDataService(delegate.getContext());
        Subscription subscription = taskDataService.removeTask().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<Boolean>() {
                            @Override
                            public void call(Boolean b) {}
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {}
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }
}