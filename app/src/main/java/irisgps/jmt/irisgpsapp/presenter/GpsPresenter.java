package irisgps.jmt.irisgpsapp.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.provider.Settings;

import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.service.gps.GpsService;
import irisgps.jmt.irisgpsapp.util.GpsUtil;
import irisgps.jmt.irisgpsapp.view.activity.VisitDetailActivity;
import irisgps.jmt.irisgpsapp.view.dialog.MessageDialog;
import irisgps.jmt.irisgpsapp.view.interface_view.CallbackMessageDialog;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class GpsPresenter {
    public static final int CODE_RESULT = 201;
    private Context ctx;
    private Intent IntentGpsService;
    MessageDialog messageDialog;
    public GpsPresenter(Context context){
        ctx = context;
        IntentGpsService = new Intent(ctx,  GpsUtil.getGpsService(ctx));
        messageDialog = new MessageDialog(ctx);
    }

    public void startGeoLocation() {
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        System.out.println("Iniciando el GeoLocation.......................................................................................................!!!");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(checkGps())
                    ctx.startService(IntentGpsService);
                else
                    alertTurnGPS();
            }
        }, 1000);
    }

    public boolean checkGps() {
        String provider = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        return provider.contains("gps");
    }
    public void alertTurnGPS(){
        messageDialog.showMessageDialog(ctx.getString(R.string.s_activar_gps), new CallbackMessageDialog() {
        @Override
        public void finish() {
                System.out.println("ola k ase");
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                ((Activity) ctx).startActivityForResult(intent, CODE_RESULT);
            }
        });
    }

    public void finishGpsService(){
        System.out.println("GpsPresenter{} -->GpsFinishing");
        System.out.println("GpsPresenter{} -->GpsFinishing");
        System.out.println("GpsPresenter{} -->GpsFinishing");
        ctx.stopService(IntentGpsService);
    }
}