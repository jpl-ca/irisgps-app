package irisgps.jmt.irisgpsapp.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;

import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.service.gcm.GcmRegistrationIdService;
import irisgps.jmt.irisgpsapp.service.http.AccessAccountService;
import irisgps.jmt.irisgpsapp.util.JMStore;
import retrofit.HttpException;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class GcmPresenter {
    Context context;
    private JMStore jmStore;
    private AccessAccountService accessAccountService;
    private CompositeSubscription compositeSubscription;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public GcmPresenter(Context context) {
        this.context = context;
        compositeSubscription = new CompositeSubscription();
        accessAccountService = new AccessAccountService(context);
        jmStore = new JMStore(context);
    }

    public void startGcmService(){
        if (checkPlayServices()) {
            Intent intent = new Intent(context, GcmRegistrationIdService.class);
            context.startService(intent);
        }else{
            Toast.makeText(context, "Device does not support!", Toast.LENGTH_LONG).show();
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog((Activity)context, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                System.out.println("This device is not supported.");
            }
            return false;
        }
        return true;
    }

    public void sendGcmId() {
        String gcm_id = getGcmId();
        if (gcm_id == null || gcm_id.equals("")) return;
        System.out.println("Sending gcm_id...!!!");
        registerGcmId(gcm_id);
        jmStore.pushBoolean(S.GCM.new_gcm_id, false);
    }

    public void saveGcmId(String gcm_id) {
        if (getGcmId().equals(gcm_id)) return;
        System.out.println("Guardando gcm->" + gcm_id);
        jmStore.pushBoolean(S.GCM.new_gcm_id, true);
        jmStore.saveString(S.GCM.gcm_id, gcm_id);
    }

    public boolean hasGcmId() {
        return jmStore.getString(S.GCM.gcm_id) != null && jmStore.getString(S.GCM.gcm_id).length() > 0;
    }

    public String getGcmId() {
        return jmStore.getString(S.GCM.gcm_id);
    }


    private void registerGcmId(String gcm_id) {
        Subscription subscription = accessAccountService.getApi().updateNotificationId(gcm_id).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<JSONObject>() {
                            @Override
                            public void call(JSONObject str) {
                                System.out.println("Gcm-->"+str);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                ConnectException connEx;
                                HttpException httpEx;
                                if (throwable instanceof ConnectException) {
                                    connEx = ((ConnectException) throwable);
                                    System.out.println(context.getString(R.string.s_error_de_conexion));
                                } else if (throwable instanceof HttpException) {
                                    httpEx = ((HttpException) throwable);
                                    System.out.println(httpEx.code());
                                    ResponseBody responseBody = httpEx.response().errorBody();
                                    try {
                                        String str = responseBody.string();
                                        System.out.println("--->" + str);
                                    } catch (IOException e) {
                                        System.out.println(context.getString(R.string.s_error_de_conexion));
                                    }
                                } else {
                                    throwable.printStackTrace();
                                    System.out.println(context.getString(R.string.s_error_de_conexion));
                                }
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }
}