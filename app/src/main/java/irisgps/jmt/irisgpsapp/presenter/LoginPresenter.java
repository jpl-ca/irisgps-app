package irisgps.jmt.irisgpsapp.presenter;

import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;

import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.TrackeableE;
import irisgps.jmt.irisgpsapp.service.db.AgentDataService;
import irisgps.jmt.irisgpsapp.service.db.VehicleDataService;
import irisgps.jmt.irisgpsapp.service.http.AccessAccountService;
import irisgps.jmt.irisgpsapp.util.JMStore;
import irisgps.jmt.irisgpsapp.view.interface_view.LoginView;
import retrofit.HttpException;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class LoginPresenter {
    LoginView delegate;
    private AccessAccountService accessAccountService;
    private AgentDataService agentDataService;
    private VehicleDataService vehicleDataService;
    private CompositeSubscription compositeSubscription;
    private JMStore jmStore;
    public LoginPresenter(LoginView delegate){
        this.delegate = delegate;
        compositeSubscription = new CompositeSubscription();
        accessAccountService = new AccessAccountService(delegate.getContext());
        agentDataService = new AgentDataService(delegate.getContext());
        vehicleDataService = new VehicleDataService(delegate.getContext());
        jmStore = new JMStore(delegate.getContext());
    }

    public void finish(){
        compositeSubscription.unsubscribe();
    }

    public void login(String user,String clave){
        if(user.contains("-")){
            loginVehicle(user,clave);
        }else{
            loginAgent(user, clave);
        }
    }

    private void loginAgent(String user, String clave) {
        Subscription subscription = accessAccountService.getApi().userLoginAgent(user, clave).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<TrackeableE>() {
                            @Override
                            public void call(TrackeableE track) {
                                System.out.println("-->Correcto");
                                jmStore.pushBoolean(JMStore.PREF_HAS_SESSION,true);
                                saveAgent(track);
                                saveToken(track.getToken());
                                saveIntervalTimeGps(track.getLocation_frequency());
                                delegate.onRequestSuccess();
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                ConnectException connEx;
                                HttpException httpEx;
                                IllegalStateException gsonEx;
                                SocketTimeoutException stEx;
                                if (throwable instanceof ConnectException) {
                                    connEx = ((ConnectException) throwable);
                                    System.out.println(delegate.getContext().getString(R.string.s_error_de_conexion));
                                    delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                }else if(throwable instanceof HttpException) {
                                    httpEx = ((HttpException) throwable);
                                    System.out.println(httpEx.code());
                                    ResponseBody responseBody = httpEx.response().errorBody();
                                    try {
                                        String str = responseBody.string();
                                        System.out.println("--->"+str);
                                        delegate.onRequestError(str);
                                    } catch (IOException e) {
                                        delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                    }
                                }else if (throwable instanceof ConnectException) {
                                    connEx = ((ConnectException) throwable);
                                    System.out.println(delegate.getContext().getString(R.string.s_error_de_conexion));
                                    delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                }else if(throwable instanceof IllegalStateException) {
                                    gsonEx =  ((IllegalStateException) throwable);
                                    System.out.println("--->" + gsonEx.getMessage());
                                    delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                }else if(throwable instanceof SocketTimeoutException) {
                                    stEx =  ((SocketTimeoutException) throwable);
                                    System.out.println("--->" + stEx.getMessage());
                                    delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                }else{
                                    delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                    throwable.printStackTrace();
                                }
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    private void loginVehicle(String plate, String clave) {
        Subscription subscription = accessAccountService.getApi().userLoginVehicle(plate, clave).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<TrackeableE>() {
                            @Override
                            public void call(TrackeableE track) {
                                System.out.println("-->Correcto");
                                jmStore.pushBoolean(JMStore.PREF_HAS_SESSION,true);
                                saveVehicle(track);
                                saveToken(track.getToken());
                                saveIntervalTimeGps(track.getLocation_frequency());
                                delegate.onRequestSuccess();
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                System.out.println("-------!!!-------");
                                ConnectException connEx;
                                HttpException httpEx;
                                if (throwable instanceof ConnectException) {
                                    connEx = ((ConnectException) throwable);
                                    System.out.println(delegate.getContext().getString(R.string.s_error_de_conexion));
                                    delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                }else if(throwable instanceof HttpException) {
                                    httpEx = ((HttpException) throwable);
                                    System.out.println(httpEx.code());
                                    ResponseBody responseBody = httpEx.response().errorBody();
                                    try {
                                        String str = responseBody.string();
                                        System.out.println("--->"+str);
                                        delegate.onRequestError(str);
                                    } catch (IOException e) {
                                        delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                    }
                                }else{
                                    delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                                    throwable.printStackTrace();
                                }
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    private void saveToken(String token) {
        System.out.println("Guardando Token ->"+token);
        jmStore.saveString(S.VAR.TOKEN, token);
    }

    private void saveUserType(String type) {
        jmStore.saveString(S.ENTITY.Type,type);
    }

    private void saveIntervalTimeGps(int i) {
        jmStore.pushIntervalTimeGps(i);
    }

    private void saveAgent(TrackeableE agent) {
        saveUserType(S.ENTITY.Agent);
        Subscription subscription = agentDataService.registerAgent(agent).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<TrackeableE>() {
                            @Override
                            public void call(TrackeableE user) {
                                System.out.println("Agent fue insertado...");
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    private void saveVehicle(TrackeableE vehicle) {
        saveUserType(S.ENTITY.Vehicle);
        Subscription subscription = vehicleDataService.registerVehicle(vehicle).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<TrackeableE>() {
                            @Override
                            public void call(TrackeableE user) {
                                System.out.println("Vehicle fue insertado...");
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }
}