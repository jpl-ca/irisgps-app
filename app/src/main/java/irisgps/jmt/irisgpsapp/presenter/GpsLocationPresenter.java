package irisgps.jmt.irisgpsapp.presenter;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.SystemClock;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.okhttp.ResponseBody;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import irisgps.jmt.irisgpsapp.broadcast.RestartGpsServiceReceiver;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.LocationHistoryE;
import irisgps.jmt.irisgpsapp.model.entity.TaskE;
import irisgps.jmt.irisgpsapp.service.db.LocationDataService;
import irisgps.jmt.irisgpsapp.service.db.TaskDataService;
import irisgps.jmt.irisgpsapp.service.gps.GpsConstants;
import irisgps.jmt.irisgpsapp.service.gps.GpsService;
import irisgps.jmt.irisgpsapp.service.http.GeolocationService;
import irisgps.jmt.irisgpsapp.util.DateUtil;
import irisgps.jmt.irisgpsapp.util.GpsUtil;
import irisgps.jmt.irisgpsapp.util.JMStore;
import irisgps.jmt.irisgpsapp.view.interface_view.GeolocationView;
import retrofit.HttpException;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class GpsLocationPresenter {
    private Context ctx;
    private CompositeSubscription compositeSubscription;
    private LocationDataService locationDataService;
    private GeolocationService geolocationService;
    private JMStore jmStore;
    private PendingIntent pendingIntent;
    private GeolocationView delegate;
    private String IS_TIME_FOR_UPDATE_LOCATION = "IS_TIME_FOR_UPDATE_LOCATION";
    private int intervalUpdateLocation;
    private int ONE_SECOND = 1000;
    private TaskDataService taskDataService;

    public GpsLocationPresenter(GeolocationView delegate){
        this.delegate = delegate;
        ctx = delegate.getContext();
        jmStore = new JMStore(ctx);
        taskDataService = new TaskDataService(ctx);
        locationDataService = new LocationDataService(ctx);
        geolocationService = new GeolocationService(ctx);
        compositeSubscription = new CompositeSubscription();
        sendToBroadcast();
    }

    public void waitForSomeSeconds() {
        int DELAY_TIME = jmStore.getIntervalTimeGps()*ONE_SECOND;
        Intent notificationIntent = new Intent(IS_TIME_FOR_UPDATE_LOCATION);
        pendingIntent = PendingIntent.getBroadcast(ctx, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        long futureInMillis = SystemClock.elapsedRealtime() + DELAY_TIME;
        AlarmManager alarmManager = (AlarmManager)ctx.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

    public int getIntervalUpdateLocation() {
        if(jmStore.getString(S.ENTITY.Type).equals(S.ENTITY.Vehicle))
            intervalUpdateLocation = 5 * ONE_SECOND;
        else intervalUpdateLocation = 10 * ONE_SECOND;
        return intervalUpdateLocation;
    }

    public void finish(){
        compositeSubscription.unsubscribe();
        if(pendingIntent!=null)pendingIntent.cancel();
    }

    public boolean registerGeoLocation(Location location){
        Intent IntentGpsService = new Intent(ctx,  GpsUtil.getGpsService(ctx));
        if(!jmStore.getBoolean(JMStore.PREF_HAS_SESSION)){
            System.out.println("A finalizar GPS_SERVICE");
            System.out.println("A finalizar GPS_SERVICE");
            ctx.stopService(IntentGpsService);
            return false;
        }
        if(location != null) {
            GpsConstants.GLOBAL_LATITUDE = location.getLatitude();
            GpsConstants.GLOBAL_LONGITUDE = location.getLongitude();
            GpsConstants.GLOBAL_SPEED = location.getSpeed();
            if(GpsConstants.GLOBAL_LATITUDE != GpsConstants.GLOBAL_LAST_LATITUDE&&GpsConstants.GLOBAL_LONGITUDE!=GpsConstants.GLOBAL_LAST_LONGITUDE){
                GpsConstants.TYPE_MESSAGE = GpsConstants.TYPE_LOCATION;
                sendToBroadcast();
                saveLocation();
                storeLocation();
                GpsConstants.GLOBAL_LAST_LATITUDE = GpsConstants.GLOBAL_LATITUDE;
                GpsConstants.GLOBAL_LAST_LONGITUDE = GpsConstants.GLOBAL_LONGITUDE;
            }
        } else {
            System.out.println("Cannot locate device without explicit Location permissions!");
        }
        return true;
    }

    private void storeLocation() {
        jmStore.setLastLocation(new LatLng(GpsConstants.GLOBAL_LATITUDE,GpsConstants.GLOBAL_LONGITUDE));
    }

    private void sendToBroadcast() {
        if(GpsConstants.GLOBAL_LATITUDE!=0&&GpsConstants.GLOBAL_LONGITUDE!=0)
            ctx.sendBroadcast(new Intent(GpsService.RECEIVE_GPS));
    }


    private void saveLocation() {
        final LocationHistoryE locationHistory = new LocationHistoryE(GpsConstants.GLOBAL_LATITUDE,GpsConstants.GLOBAL_LONGITUDE);
        Subscription subscription = taskDataService.getTask().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<TaskE>() {
                            @Override
                            public void call(TaskE task) {
                                Date start = DateUtil.parseStrToDateTime(task.getStart_date());
                                Date end = DateUtil.parseStrToDateTime(task.getEnd_date());
                                Date now = new Date();
                                if(now.getTime()>=start.getTime()&&now.getTime()<=end.getTime())
                                    locationHistory.setTask_id(task.getTask_id());
                                else if(now.getTime()>end.getTime()){
                                    deleteTask();
                                }
                                registerLocation(locationHistory);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                registerLocation(locationHistory);
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    private void registerLocation(LocationHistoryE locationHistory) {
        Subscription subscription = locationDataService.registerLocation(locationHistory).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<LocationHistoryE>() {
                            @Override
                            public void call(LocationHistoryE obj) {
                                System.out.println("Localizacion insertado-->" + obj.getLat() + "," + obj.getLng());
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    public void proccessToSendLocations() {
        Subscription subscription = locationDataService.getLocations().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<ArrayList<LocationHistoryE>>() {
                            @Override
                            public void call(ArrayList<LocationHistoryE> obj) {
                                procesarLocations(obj);
                                waitForSomeSeconds();
                                removeLocations();
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    private void forceLogin() {
        ///TODO Verificar en que momento no cancela el temporizador de envio de posicion
        ///TODO Verificar en que momento no cancela el temporizador de envio de posicion
        ///TODO Verificar en que momento no cancela el temporizador de envio de posicion
        ///TODO Verificar en que momento no cancela el temporizador de envio de posicion
        ///TODO Verificar en que momento no cancela el temporizador de envio de posicion
        System.out.println("-> GPS WILLL SHUT DOWN!");
        System.out.println("-> GPS WILLL SHUT DOWN!");
        System.out.println("-> GPS WILLL SHUT DOWN!");
        ctx.stopService(new Intent(ctx, GpsService.class));
        GpsConstants.TYPE_MESSAGE = GpsConstants.TYPE_DISCONNECT;
        ctx.sendBroadcast(new Intent(GpsService.RECEIVE_GPS));
        LogoutPresenter logout = new LogoutPresenter();
        logout.setContext(delegate.getContext());
        logout.removeAllData();
        jmStore.saveString(S.VAR.TOKEN, "");
    }

    private void removeLocations() {
        Subscription subscription = locationDataService.removeLocations().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<Boolean>() {
                            @Override
                            public void call(Boolean obj) {
                                System.out.println("Localizaciones eliminadas...");
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    private ArrayList<LocationHistoryE> removeSamePosition(ArrayList<LocationHistoryE> points) {
        ArrayList<LocationHistoryE> pp = new ArrayList<>();
        HashSet<String> single = new HashSet<>();
        for (LocationHistoryE lh: points){
            String v = lh.getLat()+","+lh.getLng();
            if(!single.contains(v)){
                single.add(v);
                pp.add(lh);
            }
        }
        System.out.println(" Se repitieron " + (points.size() - pp.size()) + " posiciones");
        return pp;
    }

    private void procesarLocations(ArrayList<LocationHistoryE> points) {
        if(points.size()==0)return;
        System.out.println("Se analizaran " + points.size() + " datos");
        ArrayList<LocationHistoryE> locationHistories = removeSamePosition(points);
        locationHistories = removePeak(locationHistories);
        LocationHistoryE ll = locationHistories.get(locationHistories.size() - 1);
        enviarGeolocations(ll);
    }

    private void enviarGeolocations(LocationHistoryE location) {
        Observable<JSONObject> observableLocation = null;
        if(location.getTask_id()==0)
            observableLocation = geolocationService.getApi().storeGeolocation(location.getLat(), location.getLng());
        else
            observableLocation = geolocationService.getApi().storeGeolocation(location.getTask_id(),location.getLat(), location.getLng());
        Subscription subscription = observableLocation.subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<JSONObject>() {
                            @Override
                            public void call(JSONObject str) {
                                System.out.println("Registrado..."+str);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                HttpException httpEx;
                                System.out.println("*-*Error");
                                if(throwable instanceof HttpException) {
                                    httpEx = ((HttpException) throwable);
                                    if(httpEx.code()==401||httpEx.code()==403){
                                        System.out.println("Error con la sesion en el servidor, Error NETWOEK ...........................*******************");
                                        System.out.println("Error con la sesion en el servidor, Error NETWOEK ...........................*******************");
                                        System.out.println("Error con la sesion en el servidor, Error NETWOEK ...........................*******************");
                                        System.out.println("Error con la sesion en el servidor, Error NETWOEK ...........................*******************");
                                        System.out.println("Error con la sesion en el servidor, Error NETWOEK ...........................*******************");
                                        System.out.println("Error con la sesion en el servidor, Error NETWOEK ...........................*******************");
                                        pendingIntent.cancel();
                                        forceLogin();
                                    }
                                    System.out.println(httpEx.code());
                                    ResponseBody responseBody = httpEx.response().errorBody();
                                    try {
                                        String str = responseBody.string();
                                        System.out.println("--->" + str);
                                    } catch (IOException e) {
                                    }
                                }else{
                                    throwable.printStackTrace();
                                }
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    private double getKmByHour(double spd) {
        return ((spd*3600)/1000);
    }

    public double calcDistance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return earthRadius * c;
    }

    private LatLng getCentroid(ArrayList<LocationHistoryE> points) {
        double longitude = 0;
        double latitude = 0;
        double maxlat = 0, minlat = 0, maxlon = 0, minlon = 0;
        int i = 0;
        for (LocationHistoryE p : points) {
            latitude = p.getLat();
            longitude = p.getLng();
            if (i == 0) {
                maxlat = latitude;
                minlat = latitude;
                maxlon = longitude;
                minlon = longitude;
            } else {
                if (maxlat < latitude)
                    maxlat = latitude;
                if (minlat > latitude)
                    minlat = latitude;
                if (maxlon < longitude)
                    maxlon = longitude;
                if (minlon > longitude)
                    minlon = longitude;
            }
            i++;
        }
        latitude = (maxlat + minlat) / 2;
        longitude = (maxlon + minlon) / 2;
        return new LatLng(latitude, longitude);
    }

    public static final int MIN_DISTANCE_DISPLACEMENT = 10;
    public ArrayList<LocationHistoryE> removePeak(ArrayList<LocationHistoryE> ListaPosicion){
        ArrayList<LocationHistoryE> res = new ArrayList<>();
        double sumaCorregidos = 0;
        LocationHistoryE[] pox2 = new LocationHistoryE[ListaPosicion.size()];
        int id = 0;
        long task_id = 0;
        for (LocationHistoryE ll : ListaPosicion){
            pox2[id++]=ll;
            if(ll.getTask_id()!=0)task_id = ll.getTask_id();
        }

        for (int i=1; i<pox2.length-1; i++){
            double ddPS = calcDistance(pox2[i-1].getLat(), pox2[i-1].getLng(), pox2[i].getLat(), pox2[i].getLng());
            double ddST = calcDistance(pox2[i].getLat(), pox2[i].getLng(), pox2[i+1].getLat(), pox2[i+1].getLng());
            double ddPT = calcDistance(pox2[i-1].getLat(), pox2[i-1].getLng(), pox2[i+1].getLat(), pox2[i+1].getLng());
            double cd = 0;
            int sz = res.size();
            if(res.size()==0||res.size()==1){
                sz = 1;
                sumaCorregidos = 0;
            }else{
                cd = calcDistance(res.get(sz-2).getLat(),res.get(sz-2).getLng(),res.get(sz-1).getLat(),res.get(sz-1).getLng());
                sumaCorregidos += cd;
            }
            double promedio = sumaCorregidos/(double)sz;
            res.add(new LocationHistoryE(task_id,pox2[i - 1].getLat(), pox2[i - 1].getLng()));
            if((ddPT<ddPS&&ddPT<ddST) || (ddPS>500&&ddST>500) || (ddPT>MIN_DISTANCE_DISPLACEMENT||ddPT>MIN_DISTANCE_DISPLACEMENT)){
                i++;
            }
        }
        if (ListaPosicion.size()<=2)return ListaPosicion;
        System.out.println(ListaPosicion.size()+" --SizeSubTot-- "+res.size());
        ArrayList <LocationHistoryE> fixPos = new ArrayList<>();
        fixPos.add(res.get(0));
        for (int i=1; i<res.size(); i++){
            LocationHistoryE a = res.get(i-1);
            LocationHistoryE b = res.get(i);
            double dist = calcDistance(a.getLat(), a.getLng(), b.getLat(), b.getLng());
            if(dist>MIN_DISTANCE_DISPLACEMENT){
            }else {
                fixPos.add(res.get(i));
            }
        }
        res = fixPos;
        fixPos = new ArrayList<>();
        fixPos.add(res.get(0));
        for (int i=1; i<res.size(); i++){
            LocationHistoryE a = res.get(i-1);
            LocationHistoryE b = res.get(i);
            double dist = calcDistance(a.getLat(), a.getLng(), b.getLat(), b.getLng());
            if(dist>MIN_DISTANCE_DISPLACEMENT){
            }else {
                fixPos.add(res.get(i));
            }
        }
        System.out.println(ListaPosicion.size()+" --SizeTot-- "+res.size() + " --- SZ:"+fixPos.size());
        return fixPos;
    }

    public void restartService() {
        ctx.sendBroadcast(new Intent(RestartGpsServiceReceiver.ACTION_RESTART));
    }

    public void checkGpsIsUpdating() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!delegate.hasFirstLocation()) {
                    System.out.println("Extrañamente no funciona el gpsssss");
                    restartService();
                }
            }
        }, 15 * 1000);
    }

    private void deleteTask() {
        TaskDataService taskDataService = new TaskDataService(delegate.getContext());
        Subscription subscription = taskDataService.removeTask().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<Boolean>() {
                            @Override
                            public void call(Boolean b) {}
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {}
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }
}