package irisgps.jmt.irisgpsapp.presenter;

import com.squareup.okhttp.ResponseBody;

import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;

import io.realm.exceptions.RealmException;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.CheckListE;
import irisgps.jmt.irisgpsapp.model.entity.TaskE;
import irisgps.jmt.irisgpsapp.model.entity.TaskVisitPointsE;
import irisgps.jmt.irisgpsapp.service.db.TaskDataService;
import irisgps.jmt.irisgpsapp.service.http.GeolocationService;
import irisgps.jmt.irisgpsapp.view.interface_view.CheckListView;
import irisgps.jmt.irisgpsapp.view.interface_view.TaskView;
import retrofit.HttpException;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class CheckListPresenter {
    CheckListView delegate;
    private GeolocationService geolocationService;
    private TaskDataService taskDataService;
    private CompositeSubscription compositeSubscription;
    public CheckListPresenter(CheckListView delegate){
        this.delegate = delegate;
        compositeSubscription = new CompositeSubscription();
        geolocationService = new GeolocationService(delegate.getContext());
        taskDataService = new TaskDataService(delegate.getContext());
    }

    public void finish(){
        compositeSubscription.unsubscribe();
    }

    public void checkListCheck(final long checklist_id, final int checkStatus){
        Observable<JSONObject> checkObservable = null;
        if(checkStatus == S.CHECKEDLIST_STATE.Checked)
             checkObservable = geolocationService.getApi().checkListCheck(checklist_id);
        else checkObservable = geolocationService.getApi().uncheckListCheck(checklist_id);

        Subscription subscription = checkObservable.
        subscribeOn(Schedulers.io()).
        observeOn(AndroidSchedulers.mainThread()).
        subscribe(
                new Action1<JSONObject>() {
                    @Override
                    public void call(JSONObject b) {
                        System.out.println("checkListCheck()--->" + b);
                        updateCheckListLocal(checklist_id, checkStatus);
                        delegate.onCheckedSuccess();
                    }
                },
                new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        ConnectException connEx;
                        HttpException httpEx;
                        if (throwable instanceof ConnectException) {
                            connEx = ((ConnectException) throwable);
                            System.out.println(delegate.getContext().getString(R.string.s_error_de_conexion));
                            delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                        } else if (throwable instanceof HttpException) {
                            httpEx = ((HttpException) throwable);
                            System.out.println(httpEx.code());
                            ResponseBody responseBody = httpEx.response().errorBody();
                            try {
                                String str = responseBody.string();
                                System.out.println("--->" + str);
                                delegate.onRequestError(str);
                            } catch (IOException e) {
                                delegate.onRequestError(delegate.getContext().getString(R.string.s_error_de_conexion));
                            }
                        }
                    }
                }
        );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    private void updateCheckListLocal(long checklist_id, int checkStatus) {
        Observable<CheckListE> checkListObservable = null;
        if(checkStatus==S.CHECKEDLIST_STATE.Checked){
            checkListObservable = taskDataService.checkListChecked(checklist_id);
        }else{
            checkListObservable = taskDataService.uncheckListChecked(checklist_id);
        }
        Subscription subscription = checkListObservable.
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<CheckListE>() {
                            @Override
                            public void call(CheckListE chck) {
                                System.out.println("A---->" + chck.getChecklist_id());
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                System.out.println("B----> BAD");
                                if (throwable instanceof RealmException) {
                                    System.out.println(":C");
                                } else {
                                    throwable.printStackTrace();
                                }
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    public void rescheduleVisitPoint(final long visit_point_id){
        rescheduleVisitPoint(visit_point_id,"");
    }
    public void rescheduleVisitPoint(final long visit_point_id,String comment){
        Subscription subscription = geolocationService.getApi().rescheduleVisitPoints(visit_point_id,comment).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<JSONObject>() {
                            @Override
                            public void call(JSONObject vp) {
                                changeVisitPointStateLocal(visit_point_id, S.VISIT_STATE.ReScheduling);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                System.out.println("BB----> BAD");
                                if (throwable instanceof RealmException) {
                                    System.out.println(":C");
                                } else {
                                    throwable.printStackTrace();
                                }
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    public void changeVisitPointStateLocal(long visit_point_id, final int new_state){
        Subscription subscription = taskDataService.setTaskVisitPointsState(visit_point_id,new_state).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<TaskVisitPointsE>() {
                            @Override
                            public void call(TaskVisitPointsE vp) {
                                delegate.onChangeStateSuccess(new_state);
                                System.out.println("AA---->" + vp.getTask_visit_id());
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                System.out.println("BB----> BAD");
                                if (throwable instanceof RealmException) {
                                    System.out.println(":C");
                                } else {
                                    throwable.printStackTrace();
                                }
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }
}