package irisgps.jmt.irisgpsapp.presenter;

import irisgps.jmt.irisgpsapp.view.dialog.MessageDialog;
import irisgps.jmt.irisgpsapp.view.dialog.MessageInputDialog;
import irisgps.jmt.irisgpsapp.view.interface_view.CallbackConfirmDialog;
import irisgps.jmt.irisgpsapp.view.interface_view.CallbackInputMessageDialog;
import irisgps.jmt.irisgpsapp.view.interface_view.ConfirmDialogView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class ConfirmDialogPresenter {
    ConfirmDialogView delegate;
    private MessageDialog messageDialog;
    private MessageInputDialog messageInputDialog;

    public ConfirmDialogPresenter(ConfirmDialogView delegate) {
        this.delegate = delegate;
        messageDialog = new MessageDialog(delegate.getContext());
        messageInputDialog = new MessageInputDialog(delegate.getContext());
    }

    public void showDialog(String message) {
        messageDialog.showConfirmDialog(message, new CallbackConfirmDialog() {
            @Override
            public void confirm() {
                delegate.clickConfirm();
            }
            @Override
            public void cancel() {
                delegate.clickCancel();
            }
        });
    }
    public void showInputDialog(String message) {
        messageInputDialog.showInputMessageDialog(message, new CallbackInputMessageDialog() {
            @Override
            public void confirm(String input) {
                delegate.clickConfirm(input);
            }
            @Override
            public void cancel() {
                delegate.clickCancel();
            }
        });
    }
}