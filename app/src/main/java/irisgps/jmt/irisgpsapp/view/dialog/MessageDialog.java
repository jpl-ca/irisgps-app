package irisgps.jmt.irisgpsapp.view.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.view.interface_view.CallbackConfirmDialog;
import irisgps.jmt.irisgpsapp.view.interface_view.CallbackMessageDialog;

/**
 * Created by JMTech-Android on 19/10/2015.
 */
public class MessageDialog {
    private AlertDialog.Builder builder;
    private AlertDialog dlg;
    private Context ctx;
    @Bind(R.id.txt_message) TextView txt_message;

    public MessageDialog(Context ctx){
        this.ctx = ctx;
        builder = new AlertDialog.Builder(ctx, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(ctx.getString(R.string.app_name));
    }

    public void hideDialog() {
        dlg.dismiss();
    }

    public void showMessageDialog(String message,final CallbackMessageDialog _cb){
        View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_message, null, false);
        ButterKnife.bind(this, view);
        txt_message.setText(message);
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE: hideDialog();_cb.finish(); break;
                }
            }
        };
        builder.setCancelable(false);
        builder.setView(view);
        builder.setPositiveButton(ctx.getString(R.string.aceptar), dialogClickListener);
        dlg = builder.create();
        dlg.show();
    }

    public void showConfirmDialog(String message,final CallbackConfirmDialog _cb){
        View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_message, null, false);
        ButterKnife.bind(this, view);
        txt_message.setText(message);
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE: _cb.confirm(); break;
                    case DialogInterface.BUTTON_NEGATIVE: _cb.cancel(); break;
                }
            }
        };
        builder.setCancelable(false);
        builder.setView(view);
        builder.setPositiveButton(ctx.getString(R.string.confirmar), dialogClickListener);
        builder.setNegativeButton(ctx.getString(R.string.cancelar), dialogClickListener);
        dlg = builder.create();
        dlg.show();
    }
}