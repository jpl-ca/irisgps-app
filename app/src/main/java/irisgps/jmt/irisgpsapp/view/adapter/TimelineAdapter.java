package irisgps.jmt.irisgpsapp.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.TaskVisitPointsE;

/**
 * Created by jmtech on 5/20/16.
 */
public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.ViewHolder> {
    private Context ctx;
    private TimelineListener timelineListener;
    private ArrayList<TaskVisitPointsE> data;
    public TimelineAdapter(Context _ctx, ArrayList<TaskVisitPointsE> data,TimelineListener timelineListener){
        ctx = _ctx;
        this.data = data;
        this.timelineListener = timelineListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_timeline, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        TaskVisitPointsE obj = data.get(position);
        holder.ll_visit_state.setBackgroundResource(getColorFromState(obj.getVisitState()));
        holder.tv_estado.setText(String.valueOf(getNameFromState(obj.getVisitState()).charAt(0)));
        String hora = obj.getTime();
        if(hora != null){
            holder.tv_tiempo.setText(hora);
        }else{
            holder.tv_tiempo.setText("23:00:00");
//            holder.tv_tiempo.setVisibility(View.GONE);
        }
        holder.tv_nombre.setText(obj.getName());
        holder.tv_direccion.setText(obj.getAddress());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timelineListener.onItemClick(position);
            }
        });
    }

    private int getColorFromState(int visitState) {
        switch (visitState){
            case S.VISIT_STATE.Scheduled: return R.color.visit_state_schedule;
            case S.VISIT_STATE.Done: return R.color.visit_state_done;
            case S.VISIT_STATE.ReScheduling: return R.color.visit_state_reschedule;
        }
        return R.color.visit_state_schedule;
    }
    private String getNameFromState(int visitState) {
        switch (visitState){
            case S.VISIT_STATE.Scheduled: return ctx.getString(R.string.s_programado);
            case S.VISIT_STATE.Done: return ctx.getString(R.string.s_realizado);
            case S.VISIT_STATE.ReScheduling: return ctx.getString(R.string.s_reprogramado);
        }
        return ctx.getString(R.string.s_programado);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_tiempo,tv_estado,tv_nombre,tv_direccion;
        private LinearLayout ll_visit_state;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_tiempo = (TextView)itemView.findViewById(R.id.tv_tiempo);
            tv_estado = (TextView)itemView.findViewById(R.id.tv_estado);
            tv_nombre = (TextView)itemView.findViewById(R.id.tv_nombre);
            tv_direccion = (TextView)itemView.findViewById(R.id.tv_direccion);
            ll_visit_state = (LinearLayout)itemView.findViewById(R.id.ll_visit_state);
        }
    }

    public interface TimelineListener{
        void onItemClick(int pos);
    }
}