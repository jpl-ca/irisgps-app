package irisgps.jmt.irisgpsapp.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.Bind;
import butterknife.ButterKnife;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.view.fragment.ProfileFragment;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class ProfileActivity extends AppCompatActivity implements ProfileFragment.OnProfileListener{
    private final ProfileFragment profileFragment = ProfileFragment.instance();
    @Bind(R.id.toolbar) protected Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        showProfileFragment();
    }

    private void showProfileFragment(){
        getSupportFragmentManager().beginTransaction().replace(R.id.container, profileFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void settitle(String title) {
        toolbar.setTitle(title);
    }
}