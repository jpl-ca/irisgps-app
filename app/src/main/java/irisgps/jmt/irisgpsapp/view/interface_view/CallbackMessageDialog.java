package irisgps.jmt.irisgpsapp.view.interface_view;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface CallbackMessageDialog {
    void finish();
}