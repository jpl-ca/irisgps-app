package irisgps.jmt.irisgpsapp.view.interface_view;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface HomeMapView {
//    void onRequestSuccess();
//    void onRequestError(Object object);
    void updatePosition(LatLng ll);
    Context getContext();
    void withoutSession();
}