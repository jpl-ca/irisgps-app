package irisgps.jmt.irisgpsapp.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.TaskE;
import irisgps.jmt.irisgpsapp.model.entity.TaskVisitPointsE;
import irisgps.jmt.irisgpsapp.presenter.HomeMapPresenter;
import irisgps.jmt.irisgpsapp.presenter.TaskPresenter;
import irisgps.jmt.irisgpsapp.view.interface_view.HomeMapView;
import irisgps.jmt.irisgpsapp.view.interface_view.TaskView;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class HomeMapFragment extends Fragment implements OnMapReadyCallback,HomeMapView,TaskView,GoogleMap.OnMarkerClickListener {
    @Bind(R.id.ll_root) RelativeLayout ll_root;
    private OnHomeMapListener presenter;
    private HomeMapPresenter homeMapPresenter;
    private TaskPresenter taskPresenter;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private float ZOOM_MAP = 11;//ZOOM_MAP = 16;

    public static TaskVisitPointsE taskToVisitInfo;
    public static ArrayList<TaskVisitPointsE> taskVisits;

    private HashMap<String,Integer> hashCli;
    private Marker markerVisit[];
    private Marker trackableMe;

    public static HomeMapFragment instance(){
        return new HomeMapFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_map, container, false);
        ButterKnife.bind(this, rootView);
        homeMapPresenter = new HomeMapPresenter(this);
        taskPresenter = new TaskPresenter(this);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        taskVisits = new ArrayList<>();
        loadCurrentTask();
        return rootView;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnHomeMapListener) {
            presenter = (OnHomeMapListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    public void loadCurrentTask() {
        taskPresenter.getCurrentTaskLocal();
        System.out.println("----->>loadCurrentTask()");
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        homeMapPresenter.startGeoLocation();
    }

    @Override
    public void onPause() {
        super.onPause();
        homeMapPresenter.stoptGeoLocation();
    }

    @Override
    public void onStop() {
        super.onStop();
        homeMapPresenter.finish();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        taskPresenter.finish();
        taskPresenter.finishAnyHandler();
    }

    public void showInfo(String s) {
        Snackbar.make(ll_root, s, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        CameraUpdate center= CameraUpdateFactory.newLatLngZoom(homeMapPresenter.getLastLocation(), ZOOM_MAP);
        mMap.moveCamera(center);
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                ZOOM_MAP = cameraPosition.zoom;
            }
        });
    }

    @Override
    public void updatePosition(LatLng ll) {
        addMyPosition(ll);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ll, ZOOM_MAP));
    }

    @Override
    public void withoutSession() {
        presenter.onLogoutSuccess();
    }

    private void addMyPosition(LatLng ll) {
        if(trackableMe!=null)trackableMe.remove();
        MarkerOptions meMrkOpt = new MarkerOptions().position(ll).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_trackable));
        trackableMe = mMap.addMarker(meMrkOpt);
    }

    @Override
    public void listTask(TaskE task) {
        taskVisits = task.getTaskVisitPoints();
        hashCli = new HashMap<>();
        if(markerVisit!=null)
            for (int ii=0;ii<markerVisit.length;ii++)
                if(markerVisit[ii]!=null)markerVisit[ii].remove();
        int i=0;
        markerVisit = new Marker[taskVisits.size()];
        for (TaskVisitPointsE visits : taskVisits){
            int visitState = visits.getVisitState();
            if(visitState==S.VISIT_STATE.Cancelled)continue;
            int ic_marker = getMarkerFromState(visitState);
            MarkerOptions markerOptions = new MarkerOptions().position(visits.getLatLng()).title(visits.getName()).icon(BitmapDescriptorFactory.fromResource(ic_marker));
            markerVisit[i] = mMap.addMarker(markerOptions);
            hashCli.put(markerVisit[i].getId(),i);
            i++;
        }
        mMap.setOnMarkerClickListener(this);
    }

    private int getMarkerFromState(int visitState) {
        switch (visitState){
            case S.VISIT_STATE.Scheduled: return R.mipmap.ic_marker_sheduled;
            case S.VISIT_STATE.Done: return R.mipmap.ic_marker_visited;
            case S.VISIT_STATE.ReScheduling: return R.mipmap.ic_marker_postponed;
        }
        return R.mipmap.ic_marker_sheduled;
    }

    @Override
    public void onRequestError(Object s) {
        showInfo(s.toString());
    }

    @Override
    public boolean onMarkerClick(Marker marker){
        if(hashCli.containsKey(marker.getId())){
            taskToVisitInfo = taskVisits.get(hashCli.get(marker.getId()));
            presenter.visitDetailActivity();
        }else{
            presenter.profileActivity();
        }
        return true;
    }

    public interface OnHomeMapListener{
        void visitDetailActivity();
        void onLogoutSuccess();
        void profileActivity();
    }
}