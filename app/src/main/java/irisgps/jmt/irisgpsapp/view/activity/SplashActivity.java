package irisgps.jmt.irisgpsapp.view.activity;

import android.os.Bundle;
import android.content.Intent;
import irisgps.jmt.irisgpsapp.R;
import android.support.v7.app.AppCompatActivity;
import irisgps.jmt.irisgpsapp.presenter.GcmPresenter;
import irisgps.jmt.irisgpsapp.view.fragment.SplashFragment;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class SplashActivity extends AppCompatActivity implements SplashFragment.OnSplashListener{
    private final SplashFragment splashFragment = SplashFragment.instance();
    private GcmPresenter gcmPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        gcmPresenter = new GcmPresenter(this);
        gcmPresenter.startGcmService();
        showFragment();
    }

    private void showFragment(){
        getSupportFragmentManager().beginTransaction().replace(R.id.container, splashFragment).commit();
    }

    @Override
    public void loginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void organizationActivity() {
        startActivity(new Intent(this, OrganizationActivity.class));
        finish();
    }

    @Override
    public void homeActivity() {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }
}