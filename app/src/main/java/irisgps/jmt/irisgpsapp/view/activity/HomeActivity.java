package irisgps.jmt.irisgpsapp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.Bind;
import butterknife.ButterKnife;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.TrackeableE;
import irisgps.jmt.irisgpsapp.presenter.GcmPresenter;
import irisgps.jmt.irisgpsapp.presenter.GpsPresenter;
import irisgps.jmt.irisgpsapp.presenter.LoginPresenter;
import irisgps.jmt.irisgpsapp.presenter.LogoutPresenter;
import irisgps.jmt.irisgpsapp.presenter.UserAccountPresenter;
import irisgps.jmt.irisgpsapp.view.fragment.HomeMapFragment;
import irisgps.jmt.irisgpsapp.view.fragment.LogoutFragment;
import irisgps.jmt.irisgpsapp.view.interface_view.LoginView;
import irisgps.jmt.irisgpsapp.view.interface_view.LogoutView;
import irisgps.jmt.irisgpsapp.view.interface_view.UserAccountView;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class HomeActivity extends AppCompatActivity implements HomeMapFragment.OnHomeMapListener,UserAccountView,LogoutView{
    private final HomeMapFragment homeMapFragment = HomeMapFragment.instance();
    private final LogoutFragment logoutFragment = LogoutFragment.instance();
    private UserAccountPresenter userAccountPresenter;
    private LogoutPresenter logoutPresenter;
    private GpsPresenter gpsPresenter;
    private GcmPresenter gcmPresenter;
    private Menu menu;
    private boolean isOnMapFragment;
    @Bind(R.id.toolbar) protected Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        userAccountPresenter = new UserAccountPresenter(this);
        logoutPresenter = new LogoutPresenter(this);
        gpsPresenter = new GpsPresenter(this);
        gcmPresenter = new GcmPresenter(this);
        toolbar.setNavigationIcon(R.mipmap.ic_bar);
        setSupportActionBar(toolbar);
        userAccountPresenter.getProfileInfo();
        gcmPresenter.sendGcmId();
        showMapFragment();
        gpsPresenter.startGeoLocation();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void showMapFragment(){
        isOnMapFragment = true;
        showAllMenuItems(true);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, homeMapFragment).commit();
    }

    private void showLogoutFragment(){
        getSupportFragmentManager().beginTransaction().replace(R.id.container, logoutFragment).commit();
        getSupportActionBar().hide();
    }

    @Override
    public void onRequestSuccess(TrackeableE usr) {
        if(usr.getType().equals(S.ENTITY.Vehicle))
            toolbar.setTitle("  " + usr.getPlate());
        else
            toolbar.setTitle("  " + usr.getFirst_name() + " " + usr.getLast_name());
    }

    @Override
    public void onRequestError(Object object) {
        System.out.println(object.toString());
    }

    @Override
    public void onLogoutSuccess() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    public void showAllMenuItems(boolean b) {
        if(menu==null)return;
        menu.findItem(R.id.action_profile).setVisible(b);
        menu.findItem(R.id.action_salir).setVisible(b);
        menu.findItem(R.id.action_timeline).setVisible(b);
    }

    @Override
    public void onBackPressed() {
        if(isOnMapFragment){
            super.onBackPressed();
        }else{
            showMapFragment();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        userAccountPresenter.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_salir) {
            showLogoutFragment();
            gpsPresenter.finishGpsService();
            logoutPresenter.logout();
            return true;
        } else if (id == R.id.action_profile) {
            profileActivity();
            return true;
        } else if (id == R.id.action_timeline) {
            timelineActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void timelineActivity() {
        startActivity(new Intent(this, TimelineActivity.class));
    }

    @Override
    public void profileActivity() {
        startActivity(new Intent(this, ProfileActivity.class));
    }

    @Override
    public void visitDetailActivity() {
        Intent it= new Intent(getContext(),VisitDetailActivity.class);
        startActivityForResult(it,VisitDetailActivity.CODE_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VisitDetailActivity.CODE_RESULT) {
            if(resultCode == RESULT_OK){
                homeMapFragment.loadCurrentTask();
            }
            if (resultCode == RESULT_CANCELED) {
            }
        }else if (requestCode == GpsPresenter.CODE_RESULT) {
            gpsPresenter.startGeoLocation();
        }
    }
}