package irisgps.jmt.irisgpsapp.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.TrackeableE;
import irisgps.jmt.irisgpsapp.presenter.UserAccountPresenter;
import irisgps.jmt.irisgpsapp.view.interface_view.UserAccountView;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class ProfileFragment extends Fragment implements UserAccountView {
    @Bind(R.id.ll_root) LinearLayout ll_root;
    @Bind(R.id.ll_profile_data) LinearLayout ll_profile_data;
    private OnProfileListener presenter;
    private UserAccountPresenter userAccountPresenter;

    private LayoutInflater inflater;

    public static ProfileFragment instance(){
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, rootView);
        this.inflater = inflater;
        userAccountPresenter = new UserAccountPresenter(this);
        userAccountPresenter.getProfileInfo();
        return rootView;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnProfileListener) {
            presenter = (OnProfileListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void showError(String s) {
        Snackbar.make(ll_root, s, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onRequestSuccess(TrackeableE usr) {
        if(usr.getType().equals(S.ENTITY.Agent)){
            presenter.settitle(getString(R.string.s_perfil_agente));
            addView(getString(R.string.p_nombre), usr.getFirst_name());
            addView(getString(R.string.p_apellido), usr.getLast_name());
        }else if(usr.getType().equals(S.ENTITY.Vehicle)){
            presenter.settitle(getString(R.string.s_perfil_vehiculo));
            addView(getString(R.string.p_placa),usr.getPlate());
            addView(getString(R.string.p_marca),usr.getBrand());
            addView(getString(R.string.p_modelo),usr.getModel());
            addView(getString(R.string.p_color),usr.getColor());
        }
    }

    public void addView(String key,String value){
        View vv  = inflater.inflate(R.layout.view_profile_data, ll_profile_data, false);
        final TextView txt_key = ((TextView)vv.findViewById(R.id.txt_key));
        final TextView txt_value = ((TextView)vv.findViewById(R.id.txt_value));
        txt_key.setText(key);
        txt_value.setText(value);
        ll_profile_data.addView(vv);
    }

    @Override
    public void onRequestError(Object object) {
        showError(object.toString());
    }

    @Override
    public void onLogoutSuccess() {}

    public interface OnProfileListener{
        void settitle(String title);
    }
}