package irisgps.jmt.irisgpsapp.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.CheckListE;
import irisgps.jmt.irisgpsapp.model.entity.TaskVisitPointsE;
import irisgps.jmt.irisgpsapp.presenter.CheckListPresenter;
import irisgps.jmt.irisgpsapp.presenter.ConfirmDialogPresenter;
import irisgps.jmt.irisgpsapp.view.activity.HomeActivity;
import irisgps.jmt.irisgpsapp.view.adapter.TimelineAdapter;
import irisgps.jmt.irisgpsapp.view.interface_view.CheckListView;
import irisgps.jmt.irisgpsapp.view.interface_view.ConfirmDialogView;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class TimelineFragment extends Fragment implements TimelineAdapter.TimelineListener{
    @Bind(R.id.ll_root) RelativeLayout ll_root;
    @Bind(R.id.rv_timeline) protected RecyclerView rv_timeline;
    LayoutInflater inflater;
    private Context context;

    private OnTimeLineListener presenter;

    private ArrayList<TaskVisitPointsE> taskVisits;

    public static TimelineFragment instance(){
        return new TimelineFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_timeline, container, false);
        ButterKnife.bind(this, rootView);
        this.inflater = inflater;
        this.context = getContext();
        taskVisits = HomeMapFragment.taskVisits;
        return rootView;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnTimeLineListener) {
            presenter = (OnTimeLineListener) context;
        } else {
            throw new ClassCastException("debe implementar On"+getClass().getSimpleName()+"Listener");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        TimelineAdapter oAdapter = new TimelineAdapter(getContext(), taskVisits, this);

        rv_timeline.setAdapter(oAdapter);
        rv_timeline.setHasFixedSize(true);
        rv_timeline.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    public void showError(String s) {
        Snackbar.make(ll_root, s, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(int pos) {
        HomeMapFragment.taskToVisitInfo = taskVisits.get(pos);
        presenter.visitDetailActivity();
    }

    public interface OnTimeLineListener{
        void visitDetailActivity();
    }
}