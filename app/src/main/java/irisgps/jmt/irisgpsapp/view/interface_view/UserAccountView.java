package irisgps.jmt.irisgpsapp.view.interface_view;

import android.content.Context;

import irisgps.jmt.irisgpsapp.model.entity.TrackeableE;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface UserAccountView {
    void onRequestSuccess(TrackeableE veh);
    void onRequestError(Object object);
    void onLogoutSuccess();
    Context getContext();
}