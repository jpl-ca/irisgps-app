package irisgps.jmt.irisgpsapp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.Bind;
import butterknife.ButterKnife;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.TrackeableE;
import irisgps.jmt.irisgpsapp.presenter.UserAccountPresenter;
import irisgps.jmt.irisgpsapp.view.fragment.TimelineFragment;
import irisgps.jmt.irisgpsapp.view.fragment.VisitDetailFragment;
import irisgps.jmt.irisgpsapp.view.interface_view.UserAccountView;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class TimelineActivity extends AppCompatActivity implements UserAccountView,TimelineFragment.OnTimeLineListener{
    private final TimelineFragment timelineFragment = TimelineFragment.instance();
    private UserAccountPresenter userAccountPresenter;
    private Menu menu;
    @Bind(R.id.toolbar) protected Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        userAccountPresenter = new UserAccountPresenter(this);
        userAccountPresenter.getProfileInfo();
        toolbar.setTitle("  ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        showVisistDetailFragment();
    }

    private void showVisistDetailFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, timelineFragment).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onRequestSuccess(TrackeableE usr) {
        if(usr.getType().equals(S.ENTITY.Vehicle))
            toolbar.setTitle("  " + usr.getPlate());
        else
            toolbar.setTitle("  " + usr.getFirst_name() + " " + usr.getLast_name());
    }

    @Override
    public void visitDetailActivity() {
        Intent it= new Intent(getContext(),VisitDetailActivity.class);
        startActivityForResult(it,VisitDetailActivity.CODE_RESULT);
    }

    @Override
    public void onRequestError(Object object) {
        System.out.println(object.toString());
    }

    @Override
    public void onLogoutSuccess() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}