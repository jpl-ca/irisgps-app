package irisgps.jmt.irisgpsapp.view.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.view.interface_view.CallbackConfirmDialog;
import irisgps.jmt.irisgpsapp.view.interface_view.CallbackMessageDialog;

/**
 * Created by JMTech-Android on 19/10/2015.
 */
public class LoadingDialog {
    private AlertDialog.Builder builder;
    private AlertDialog dlg;
    private Context ctx;
    @Bind(R.id.txt_message) TextView txt_message;
    @Bind(R.id.loading_spinner) ProgressBar loading_spinner;

    public LoadingDialog(Context ctx){
        this.ctx = ctx;
        builder = new AlertDialog.Builder(ctx, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(ctx.getString(R.string.app_name));
    }

    public void hideDialog() {
        dlg.dismiss();
    }

    public void showLoadingDialog(String message){
        View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_loading, null, false);
        ButterKnife.bind(this, view);
        loading_spinner.getIndeterminateDrawable().setColorFilter(ctx.getResources().getColor(R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY);
        txt_message.setText(message);
        builder.setCancelable(false);
        builder.setView(view);
        dlg = builder.create();
        dlg.show();
    }
}