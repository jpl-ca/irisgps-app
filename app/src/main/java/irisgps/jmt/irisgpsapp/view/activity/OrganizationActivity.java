package irisgps.jmt.irisgpsapp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.view.fragment.OrganizationFragment;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class OrganizationActivity extends AppCompatActivity implements OrganizationFragment.OnOrganizationListener{
    private final OrganizationFragment organizationFragment = OrganizationFragment.instance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organization);
        showFragment();
    }

    private void showFragment(){
        getSupportFragmentManager().beginTransaction().replace(R.id.container, organizationFragment).commit();
    }

    @Override
    public void nextActivity() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}