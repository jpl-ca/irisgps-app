package irisgps.jmt.irisgpsapp.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import irisgps.jmt.irisgpsapp.R;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class LogoutFragment extends Fragment{

    public static LogoutFragment instance(){
        return new LogoutFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_splash, container, false);
        return rootView;
    }
}