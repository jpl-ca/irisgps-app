package irisgps.jmt.irisgpsapp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.view.fragment.LoginFragment;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class LoginActivity extends AppCompatActivity implements LoginFragment.OnLoginListener{
    private final LoginFragment loginFragment = LoginFragment.instance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        showFragment();
    }

    private void showFragment(){
        getSupportFragmentManager().beginTransaction().replace(R.id.container, loginFragment).commit();
    }

    @Override
    public void nextActivity() {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }
}