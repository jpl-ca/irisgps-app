package irisgps.jmt.irisgpsapp.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.presenter.OrganizationPresenter;
import irisgps.jmt.irisgpsapp.view.interface_view.OrganizationView;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class OrganizationFragment extends Fragment implements OrganizationView {
    @Bind(R.id.txt_organization) AppCompatEditText txt_organization;
    @Bind(R.id.btn_ingresar) Button btn_ingresar;
    @Bind(R.id.ll_root) LinearLayout ll_root;
    private OnOrganizationListener presenter;
    private OrganizationPresenter organizationPresenter;

    public static OrganizationFragment instance(){
        return new OrganizationFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_organizacion, container, false);
        ButterKnife.bind(this, rootView);
        organizationPresenter = new OrganizationPresenter(this);
        return rootView;
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        if (activity instanceof OnOrganizationListener) {
            presenter = (OnOrganizationListener) activity;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @OnClick(R.id.btn_ingresar)
    public void validar(){
        String organization = txt_organization.getText().toString();
        organizationPresenter.validateOrganization(organization);
        enableFields(false);
    }

    private void enableFields(boolean b) {
        btn_ingresar.setEnabled(b);
        txt_organization.setEnabled(b);
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        organizationPresenter.finish();
    }

    public void showInfo(String s) {
        Snackbar.make(ll_root, s, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void isOrganization(boolean b) {
        if (b) {
            showInfo(getString(R.string.s_su_organizacion_fue_registrado));
            presenter.nextActivity();
        }else{
            enableFields(true);
            showInfo(getString(R.string.s_su_organizacion_no_esta_registrado));
        }
    }

    @Override
    public void onRequestError(Object object) {
        showInfo(object.toString());
        enableFields(true);
    }

    public interface OnOrganizationListener{
        void nextActivity();
    }
}