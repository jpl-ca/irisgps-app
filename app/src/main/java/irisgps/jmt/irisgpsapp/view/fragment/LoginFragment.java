package irisgps.jmt.irisgpsapp.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.presenter.LoginPresenter;
import irisgps.jmt.irisgpsapp.view.interface_view.LoginView;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class LoginFragment extends Fragment implements LoginView {
    @Bind(R.id.txt_email) AppCompatEditText txt_email;
    @Bind(R.id.txt_password) AppCompatEditText txt_password;
    @Bind(R.id.btn_ingresar) Button btn_ingresar;
    @Bind(R.id.ll_root) LinearLayout ll_root;
    private OnLoginListener presenter;
    private LoginPresenter loginPresenter;

    public static LoginFragment instance(){
        return new LoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, rootView);
        loginPresenter = new LoginPresenter(this);
        txt_email.setText("ABC-123");
        txt_password.setText("password");
        return rootView;
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        if (activity instanceof OnLoginListener) {
            presenter = (OnLoginListener) activity;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @OnClick(R.id.btn_ingresar)
    public void login(){
        String email = txt_email.getText().toString();
        String password = txt_password.getText().toString();
        enableFields(false);
        loginPresenter.login(email, password);
    }

    private void enableFields(boolean b) {
        txt_email.setEnabled(b);
        txt_password.setEnabled(b);
        btn_ingresar.setEnabled(b);
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        loginPresenter.finish();
    }

    public void showError(String s) {
        Snackbar.make(ll_root, s, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onRequestSuccess() {
        presenter.nextActivity();
    }

    @Override
    public void onRequestError(Object object) {
        showError(object.toString());
        enableFields(true);
    }

    public interface OnLoginListener{
        void nextActivity();
    }
}