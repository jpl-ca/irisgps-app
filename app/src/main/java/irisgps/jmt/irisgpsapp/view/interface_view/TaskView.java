package irisgps.jmt.irisgpsapp.view.interface_view;

import android.content.Context;
import irisgps.jmt.irisgpsapp.model.entity.TaskE;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface TaskView {
    void listTask(TaskE task);
    void onRequestError(Object object);
    Context getContext();
}