package irisgps.jmt.irisgpsapp.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.CheckListE;
import irisgps.jmt.irisgpsapp.model.entity.TaskVisitPointsE;
import irisgps.jmt.irisgpsapp.presenter.CheckListPresenter;
import irisgps.jmt.irisgpsapp.presenter.ConfirmDialogPresenter;
import irisgps.jmt.irisgpsapp.view.interface_view.CheckListView;
import irisgps.jmt.irisgpsapp.view.interface_view.ConfirmDialogView;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class VisitDetailFragment extends Fragment implements CheckListView{
    @Bind(R.id.ll_root) RelativeLayout ll_root;
    @Bind(R.id.txt_cliente) TextView txt_cliente;
    @Bind(R.id.txt_telefono) TextView txt_telefono;
    @Bind(R.id.txt_direccion) TextView txt_direccion;
    @Bind(R.id.txt_referencia) TextView txt_referencia;
    @Bind(R.id.lv_checklist) LinearLayout lv_checklist;
    @Bind(R.id.tv_state_visit_point) TextView tv_state_visit_point;
    @Bind(R.id.btnSolicitarReprogramado) Button btnSolicitarReprogramado;
    LayoutInflater inflater;
    private Context context;
    private CheckBox checkBoxList[];
    private ConfirmDialogPresenter confirmDialogPresenter;
    CheckListPresenter checkListPresenter;
    private TaskVisitPointsE taskToVisitInfo;
    private OnVisitDetailListener presenter;
    private boolean boxWasCheckedProgrammatically = false;

    public static VisitDetailFragment instance(){
        return new VisitDetailFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_visit_detail, container, false);
        ButterKnife.bind(this, rootView);
        taskToVisitInfo = HomeMapFragment.taskToVisitInfo;
        checkListPresenter = new CheckListPresenter(this);
        this.inflater = inflater;
        this.context = getContext();
        return rootView;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnVisitDetailListener) {
            presenter = (OnVisitDetailListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        txt_cliente.setText(taskToVisitInfo.getName());
        txt_telefono.setText(taskToVisitInfo.getPhone());
        txt_direccion.setText(taskToVisitInfo.getAddress());
        txt_referencia.setText(taskToVisitInfo.getReference());
        int idx = 0;
        checkBoxList = new CheckBox[taskToVisitInfo.getCheckList().size()];
        for (CheckListE check : taskToVisitInfo.getCheckList()){
            setCheckList(idx++,check);
        }
        setState(taskToVisitInfo.getVisitState());
    }

    private void setState(int visitState) {
        if(visitState==S.VISIT_STATE.Done){
            tv_state_visit_point.setText(getString(R.string.s_realizado));
            btnSolicitarReprogramado.setVisibility(View.GONE);
        }else if(visitState==S.VISIT_STATE.Scheduled){
            tv_state_visit_point.setText(getString(R.string.s_programado));
            btnSolicitarReprogramado.setVisibility(View.VISIBLE);

            boolean isCheckedOne = false;
            for (int i=0;i<checkBoxList.length;i++)
                if(checkBoxList[i].isChecked())
                    isCheckedOne = true;
            if(isCheckedOne)btnSolicitarReprogramado.setVisibility(View.GONE);
            else btnSolicitarReprogramado.setVisibility(View.VISIBLE);
        }else if(visitState==S.VISIT_STATE.ReScheduling){
            for (int i=0;i<checkBoxList.length;i++)checkBoxList[i].setEnabled(false);
            tv_state_visit_point.setText(getString(R.string.s_reprogramado));
            btnSolicitarReprogramado.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btnSolicitarReprogramado)
    protected void solicitarReprogramado() {
        confirmDialogPresenter = new ConfirmDialogPresenter(new ConfirmDialogView() {
            @Override
            public void clickConfirm() {
                checkListPresenter.rescheduleVisitPoint(taskToVisitInfo.getTask_visit_id());
            }
            @Override
            public void clickConfirm(String input) {
                checkListPresenter.rescheduleVisitPoint(taskToVisitInfo.getTask_visit_id(),input);
            }
            @Override
            public void clickCancel() {}
            @Override
            public Context getContext() {
                return context;
            }
        });
        confirmDialogPresenter.showInputDialog(context.getString(R.string.s_se_guardara_como_reprogramado));
    }

    private void setCheckList(final int idx, final CheckListE check) {
        View vv  = inflater.inflate(R.layout.lv_checklist, lv_checklist, false);
        final CheckBox cb_checked = ((CheckBox)vv.findViewById(R.id.cb_checked));
        checkBoxList[idx] = cb_checked;
        final TextView tv_descripcion = ((TextView)vv.findViewById(R.id.tv_descripcion));
        tv_descripcion.setText(check.getDescription());
        boolean checked = check.getChecked()== S.CHECKEDLIST_STATE.Checked;
        cb_checked.setChecked(checked);
        cb_checked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!boxWasCheckedProgrammatically){
                    if (b){
                        checkListDone(idx,check.getChecklist_id(), cb_checked);
                    }else{
                        unCheckListDone(idx, check.getChecklist_id(), cb_checked);
                    }
                }else{
                    System.out.println("Perfectttt");
                }
                boxWasCheckedProgrammatically = false;
            }
        });
        lv_checklist.addView(vv);
    }

    private void unCheckListDone(final int idx, final long checkListId, final CheckBox cb_checked) {
        confirmDialogPresenter = new ConfirmDialogPresenter(new ConfirmDialogView() {
            @Override
            public void clickConfirm() {
                taskToVisitInfo.getCheckList().get(idx).setChecked(S.CHECKEDLIST_STATE.Unchecked);
                checkListPresenter.checkListCheck(checkListId, S.CHECKEDLIST_STATE.Unchecked);
            }

            @Override
            public void clickConfirm(String input) {}

            @Override
            public void clickCancel() {
                boxWasCheckedProgrammatically = true;
                cb_checked.setChecked(true);
            }
            @Override
            public Context getContext() {
                return context;
            }
        });
        confirmDialogPresenter.showDialog(context.getString(R.string.s_confirma_se_tarea_no_fue_completado));
    }

    private void checkListDone(final int idx, final long checkListId, final CheckBox cb_checked) {
        confirmDialogPresenter = new ConfirmDialogPresenter(new ConfirmDialogView() {
            @Override
            public void clickConfirm() {
                taskToVisitInfo.getCheckList().get(idx).setChecked(S.CHECKEDLIST_STATE.Checked);
                checkListPresenter.checkListCheck(checkListId, S.CHECKEDLIST_STATE.Checked);
            }

            @Override
            public void clickConfirm(String input) {}

            @Override
            public void clickCancel() {
                boxWasCheckedProgrammatically = true;
                cb_checked.setChecked(false);
            }
            @Override
            public Context getContext() {
                return context;
            }
        });
        confirmDialogPresenter.showDialog(context.getString(R.string.s_confirma_se_realizo_la_tarea));
    }

    private void updateStatus() {
        presenter.sendResultSuccess();
        for (CheckListE check : taskToVisitInfo.getCheckList()){
            if(check.getChecked()==S.CHECKEDLIST_STATE.Unchecked){
                checkListPresenter.changeVisitPointStateLocal(taskToVisitInfo.getTask_visit_id(), S.VISIT_STATE.Scheduled);
                return;
            }
        }
        checkListPresenter.changeVisitPointStateLocal(taskToVisitInfo.getTask_visit_id(), S.VISIT_STATE.Done);
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    public void showError(String s) {
        Snackbar.make(ll_root, s, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onCheckedSuccess() {
        updateStatus();
    }

    @Override
    public void onChangeStateSuccess(int new_state) {
        setState(new_state);
    }

    @Override
    public void onRequestError(Object object) {
    }

    public interface OnVisitDetailListener{
        void sendResultSuccess();
    }
}