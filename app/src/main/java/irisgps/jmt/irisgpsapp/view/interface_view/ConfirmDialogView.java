package irisgps.jmt.irisgpsapp.view.interface_view;

import android.content.Context;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface ConfirmDialogView {
    void clickConfirm();
    void clickConfirm(String input);
    void clickCancel();
    Context getContext();
}