package irisgps.jmt.irisgpsapp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.Bind;
import butterknife.ButterKnife;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.model.entity.TrackeableE;
import irisgps.jmt.irisgpsapp.presenter.GpsPresenter;
import irisgps.jmt.irisgpsapp.presenter.LogoutPresenter;
import irisgps.jmt.irisgpsapp.presenter.UserAccountPresenter;
import irisgps.jmt.irisgpsapp.view.fragment.LogoutFragment;
import irisgps.jmt.irisgpsapp.view.fragment.VisitDetailFragment;
import irisgps.jmt.irisgpsapp.view.interface_view.LogoutView;
import irisgps.jmt.irisgpsapp.view.interface_view.UserAccountView;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class VisitDetailActivity extends AppCompatActivity implements LogoutView,UserAccountView,VisitDetailFragment.OnVisitDetailListener{
    private final LogoutFragment logoutFragment = LogoutFragment.instance();
    private final VisitDetailFragment visitDetailFragment = VisitDetailFragment.instance();
    private LogoutPresenter logoutPresenter;
    private UserAccountPresenter userAccountPresenter;
    private GpsPresenter gpsPresenter;
    private Menu menu;
    public static final int CODE_RESULT = 101;
    @Bind(R.id.toolbar) protected Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        logoutPresenter = new LogoutPresenter(this);
        gpsPresenter = new GpsPresenter(this);
        userAccountPresenter = new UserAccountPresenter(this);
        userAccountPresenter.getProfileInfo();
        toolbar.setTitle("  " + getString(R.string.s_visita));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        showVisistDetailFragment();
    }

    private void showVisistDetailFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, visitDetailFragment).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void showLogoutFragment(){
        getSupportFragmentManager().beginTransaction().replace(R.id.container, logoutFragment).commit();
        getSupportActionBar().hide();
    }

    @Override
    public void onRequestSuccess(TrackeableE usr) {
        if(usr.getType().equals(S.ENTITY.Vehicle))
            toolbar.setTitle("  " + usr.getPlate());
        else
            toolbar.setTitle("  " + usr.getFirst_name() + " " + usr.getLast_name());
    }

    @Override
    public void onRequestError(Object object) {
        System.out.println(object.toString());
    }

    @Override
    public void onLogoutSuccess() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menu.findItem(R.id.action_profile).setVisible(false);
        menu.findItem(R.id.action_timeline).setVisible(false);
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        logoutPresenter.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_salir) {
            showLogoutFragment();
            gpsPresenter.finishGpsService();
            logoutPresenter.logout();
            return true;
        }else if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void sendResultSuccess() {
        Intent returnIntent = new Intent();
        setResult(RESULT_OK,returnIntent);
    }
}