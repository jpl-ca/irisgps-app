package irisgps.jmt.irisgpsapp.view.interface_view;

import android.content.Context;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface OrganizationView {
    void isOrganization(boolean b);
    void onRequestError(Object object);
    Context getContext();
}