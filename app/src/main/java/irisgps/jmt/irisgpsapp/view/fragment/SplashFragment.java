package irisgps.jmt.irisgpsapp.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import irisgps.jmt.irisgpsapp.R;
import irisgps.jmt.irisgpsapp.model.S;
import irisgps.jmt.irisgpsapp.presenter.ConfirmDialogPresenter;
import irisgps.jmt.irisgpsapp.presenter.SplashPresenter;
import irisgps.jmt.irisgpsapp.view.dialog.MessageDialog;
import irisgps.jmt.irisgpsapp.view.interface_view.CallbackMessageDialog;
import irisgps.jmt.irisgpsapp.view.interface_view.ConfirmDialogView;
import irisgps.jmt.irisgpsapp.view.interface_view.SplashView;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class SplashFragment extends Fragment implements SplashView {
    private OnSplashListener presenter;
    private SplashPresenter splashPresenter;
    @Bind(R.id.rl_root) RelativeLayout ll_root;

    public static SplashFragment instance(){
        return new SplashFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_splash, container, false);
        splashPresenter = new SplashPresenter(this);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        if (activity instanceof OnSplashListener) {
            this.presenter = (OnSplashListener) activity;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                splashPresenter.getInfo();
            }
        }, 500);
    }

    @Override
    public void onStop() {
        super.onStop();
        splashPresenter.finish();
    }

    @Override
    public void onSessionSuccess() {
        presenter.homeActivity();
    }

    @Override
    public void onSessionError(boolean hasOrganization, Object object) {
        if(hasOrganization)
            presenter.loginActivity();
        else
            presenter.organizationActivity();
    }

    @Override
    public void onRequestError(Object object) {
//        showInfo(object.toString());
        MessageDialog messageDialog = new MessageDialog(getContext());
        messageDialog.showMessageDialog(object.toString(), new CallbackMessageDialog() {
            @Override
            public void finish() {
                presenter.finish();
            }
        });
    }

    public void showInfo(String s) {
        Snackbar.make(ll_root, s, Snackbar.LENGTH_LONG).show();
    }

    public interface OnSplashListener{
        void loginActivity();
        void organizationActivity();
        void homeActivity();
        void finish();
    }
}